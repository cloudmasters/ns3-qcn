#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
#include "ns3/log.h"
#include "ns3/netanim-module.h"
#include "ns3/net-device.h"
#include "ns3/bridge-helper.h"
#include "ns3/bridge-net-device.h"
#include "ns3/csma-module.h"
#include "ns3/csma-net-device.h"
#include "ns3/queue.h"
#include "ns3/qcn-cp-queue.h"
#include "ns3/qcn-rp-queue.h"
#include "ns3/qcn-fb-proto.h"
#include "ns3/callback.h"
#include "ns3/tpo_switch.h"
#include "ns3/tpo_cabinet.h"
#include "ns3/tpo_host.h"
#include "ns3/tpo_guest.h"

using namespace ns3;
using namespace std;

NS_LOG_COMPONENT_DEFINE ("AdvTreeCongestionTopology");

// Step by step initialization:
//=================================================================================================================
// 0. Enable logging
// 1. Create topology
//    1.1. Create Cabinets [ i.e. hosts, servers & access switches ]
//    1.2  Create Aggregation & Core switches
//    1.3  Connect Switches: Access to Aggregation to Core
//    1.4  Create Core uplink cabinet & connect it to core switches
// 2. Traffic configuration
//    2.1 Create Applications
//    2.2 Schedule applications
// 3. Start simulation
//=================================================================================================================

int
main (int argc, char *argv[])
{
	  bool verbose = true;

	  CommandLine cmd;
	  cmd.AddValue ("verbose", "Tell application to log if true", verbose);

	  cmd.Parse (argc,argv);

	  /* ... */


	Time::SetResolution (Time::NS);

	//The Logging
	//-----------
	//	LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
	//	LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);
	//	LogComponentEnable("CsmaNetDevice", LOG_LEVEL_INFO);
	//	LogComponentEnable("DropTailQueue", LOG_LEVEL_ALL);
	//	LogComponentEnable("QCNCPQueue", LOG_LEVEL_ALL);
	//	LogComponentEnable("CsmaNetDevice", LOG_LEVEL_ALL);

	Simulator::Run ();
	Simulator::Destroy ();
	return 0;

}
