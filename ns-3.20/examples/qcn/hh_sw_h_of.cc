
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
#include "ns3/log.h"
#include "ns3/netanim-module.h"
#include "ns3/net-device.h"
//#include "ns3/bridge-helper.h"
//#include "ns3/bridge-net-device.h"
#include "ns3/openflow-switch-helper.h"
#include "ns3/openflow-switch-net-device.h"
#include "ns3/csma-module.h"
#include "ns3/csma-net-device.h"
#include "ns3/queue.h"
#include "ns3/qcn-cp-queue.h"
#include "ns3/qcn-rp-queue.h"
#include "ns3/qcn-fb-proto.h"
#include "ns3/callback.h"

using namespace ns3;
using namespace std;

NS_LOG_COMPONENT_DEFINE ("SimpleCongestionTopology");

// The topology
//=============================================================
//
//(H0)  csma_dev-----+
//	             |
//         ********  |  *****************************
//	   *         |              THE SWITCH(sw0) *
//	   *    csma_dev------+                     *
//	   *                  |                     *
//	   *                  |                     *
//	   *                  |                     *
//	   *                bridge_dev-----csma_dev-----dev(Hn)
//	   *                  |                     *
//	   *                  |                     *
//	   *                  |                     *
//	   *    csma_dev------+                     *  
//	   *          |                             *
//	   *********  |  ****************************
//                    |    
//(H1)  csma_dev-----+    |...etc
//	  ...             |
//(Hn-1)csma_dev----------+
//

#define USE_UDP_APP
#define N_HOSTS 15

#define QCN_ACTIVE
#define QCN_RP_CALLBACK

#define UTP_DELAY 50		// 5ns/m => 50m=50ns - channel delay
#define LINK_RATE "1Gbps"	// Link/Channel Data Rate
#define MTU 1500		// 1500Bytes
#define QUEUE_LENGTH MTU*100	// 100 frames of 1500Bytes (MTU)

int
main (int argc, char *argv[])
{
	Time::SetResolution (Time::NS);

	NodeContainer swNC;
	NodeContainer hostNC;

//The Logging
//-----------
//	LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
//	LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);
//	LogComponentEnable("CsmaNetDevice", LOG_LEVEL_INFO);
//	LogComponentEnable("DropTailQueue", LOG_LEVEL_ALL);
//	LogComponentEnable("QCNCPQueue", LOG_LEVEL_ALL);
//	LogComponentEnable("CsmaNetDevice", LOG_LEVEL_ALL);

//Define Switches, Hosts
//----------------------
	swNC.Create(1);
	hostNC.Create(N_HOSTS);

//The Topology
//------------
	Ptr<CsmaChannel> channel[N_HOSTS];	// A link for each host

	Ptr<CsmaNetDevice> swCsmaND[N_HOSTS];	// A device for each link with each host
	Ptr<CsmaNetDevice> hostCsmaND[N_HOSTS];	// A device for each link with the SW

#if defined(QCN_ACTIVE)
	Ptr<DropTailQueue> swQueue[N_HOSTS-1];	// A queue for each SW device
	Ptr<QCNCPQueue>    swQueue_qcn;		// One congested controlled queue

	Ptr<QCNRPQueue>    hostQueue[N_HOSTS];	// Reaction point queues from hosts
#else
	Ptr<DropTailQueue> swQueue[N_HOSTS];
	Ptr<DropTailQueue> hostQueue[N_HOSTS];	// A queue for each host device
#endif

	// Create channels
	for(int i=0; i<N_HOSTS; i++){
		channel[i] = CreateObject <CsmaChannel> ();
		channel[i]->SetAttribute ("DataRate", StringValue (LINK_RATE));
		channel[i]->SetAttribute ("Delay", TimeValue (NanoSeconds (UTP_DELAY)));
	}

	// Create SW/CP queues
	for(int i=0; i<N_HOSTS; i++){
#if defined(QCN_ACTIVE)
		// SW - CP queues
		//---------------
		if (i == (N_HOSTS-1))  {
			// congested controlled
			swQueue_qcn = CreateObject <QCNCPQueue> ();
			swQueue_qcn->SetAttribute("MaxBytes", UintegerValue (QUEUE_LENGTH));
			swQueue_qcn->ActiveCP(true);
		} else {
			swQueue[i] = CreateObject <DropTailQueue> ();
			swQueue[i]->SetAttribute("Mode", StringValue ("QUEUE_MODE_BYTES"));
			swQueue[i]->SetAttribute("MaxBytes", UintegerValue (QUEUE_LENGTH));
		}
#else
		swQueue[i] = CreateObject <DropTailQueue> ();
		swQueue[i]->SetAttribute("Mode", StringValue ("QUEUE_MODE_BYTES"));
		swQueue[i]->SetAttribute("MaxBytes", UintegerValue (QUEUE_LENGTH));
#endif
	}

	// Create HOST/RP queues
	for(int i=0; i<N_HOSTS; i++){
#if defined(QCN_ACTIVE)
		hostQueue[i] = CreateObject <QCNRPQueue> ();
		hostQueue[i]->SetAttribute("MaxBytes", UintegerValue (QUEUE_LENGTH));
		hostQueue[i]->ActiveRP(true);
#else
		hostQueue[i] = CreateObject <DropTailQueue> ();
		hostQueue[i]->SetAttribute("Mode", StringValue ("QUEUE_MODE_BYTES"));
		hostQueue[i]->SetAttribute("MaxBytes", UintegerValue (QUEUE_LENGTH));
#endif
	}

	// Create CSMA Net Devices
	for (int i=0; i<N_HOSTS; i++) {
		Mac48Address addr;

		addr = Mac48Address::Allocate ();
		swCsmaND[i] = CreateObject <CsmaNetDevice> ();
		swCsmaND[i]->SetAttribute("Mtu", UintegerValue (MTU));
		swCsmaND[i]->SetAddress (addr);
		std::cout  << "sw[" << i  << "] " << addr << std::endl;

		addr = Mac48Address::Allocate ();
		hostCsmaND[i] = CreateObject <CsmaNetDevice> ();
		hostCsmaND[i]->SetAttribute("Mtu", UintegerValue (MTU));
		hostCsmaND[i]->SetAddress (addr);
		std::cout  << "host[" << i  << "] " << addr << std::endl;
	}

	// Attach CSMA Net Devices to nodes
	for (int i=0; i<N_HOSTS; i++) {
		Ptr<Node> swNode = swNC.Get(0);
		swNode->AddDevice (swCsmaND[i]);

		Ptr<Node> hostNode = hostNC.Get(i);
		hostNode->AddDevice (hostCsmaND[i]);
	}

	// Attach queues to CSMA Net devs
	for (int i=0; i<N_HOSTS; i++) {
		// SW queues
#if defined(QCN_ACTIVE)
		if (i == N_HOSTS-1){
			Ptr<NetDevice> swNetDev = swCsmaND[i]->GetObject<NetDevice> ();
			swCsmaND[i]->SetQueue(swQueue_qcn);
			swQueue_qcn->SetNetDev(swNetDev);
                }
		else
			swCsmaND[i]->SetQueue(swQueue[i]);
#else
		swCsmaND[i]->SetQueue(swQueue[i]);
#endif
		// Host queues
		hostCsmaND[i]->SetQueue(hostQueue[i]);
		Ptr<NetDevice> hostNetDev = hostCsmaND[i]->GetObject<NetDevice> ();
		hostQueue[i]->SetNetDev(hostNetDev);
	}

	// Attach links
	for (int i=0; i<N_HOSTS; i++) {
		// Attach Host links
		channel[i]->Attach(swCsmaND[i]);
		swCsmaND[i]->Attach(channel[i]);

		// Attach SW links
		channel[i]->Attach(hostCsmaND[i]);
		hostCsmaND[i]->Attach(channel[i]);
	}

	// Register QCN RP callback to inspect received Fbs
#if defined(QCN_ACTIVE) && defined(QCN_RP_CALLBACK)
	for (int i=0; i<N_HOSTS; i++) {
		Ptr<Node> hostNode = hostNC.Get(i);
		Ptr<NetDevice> hostNetDev = hostCsmaND[i]->GetObject<NetDevice> ();
		hostNode->RegisterProtocolHandler(
				MakeCallback(&ns3::QCNRPQueue::Rcv_Fbfrm, hostQueue[i]),
				(uint16_t)QCN_PROTO_NO,
				hostNetDev,
				false);
	}
#endif

//The Bridge
//----------
	Ptr<ns3::ofi::LearningController> controller = CreateObject<ns3::ofi::LearningController> ();
	controller->SetAttribute ("ExpirationTime", TimeValue(ns3::Seconds(1)));

//	BridgeHelper swBridgeHelper;
	OpenFlowSwitchHelper swBridgeHelper;

	NetDeviceContainer swBridgePorts;

	//Create a container with all of the ports that belong to the bridge
	for(int i=0; i<N_HOSTS; i++) {
		Ptr<NetDevice> swPort = swCsmaND[i]->GetObject<NetDevice> ();
		swBridgePorts.Add(swPort);
	}
	//install the bridge in the switch and add the ports to it
	Ptr<Node> swNode = swNC.Get(0);
	swBridgeHelper.Install(swNode, swBridgePorts, controller);

//The Internet Stack
//------------------
	InternetStackHelper internet;
	internet.Install(hostNC);
	Ipv4AddressHelper addrBase;
	addrBase.SetBase("10.1.1.0", "255.255.255.0");
	Ipv4InterfaceContainer hIf;

	for (int i=0; i<N_HOSTS; i++) {
		Ptr<NetDevice> hostNetDev = hostCsmaND[i]->GetObject<NetDevice> ();
		hIf.Add(addrBase.Assign(hostNetDev));
	}

//The Applications
//----------------
#if defined(USE_UDP_APP)
	//The Server
	UdpEchoServerHelper echoSrv(9);
	ApplicationContainer appSrv = echoSrv.Install(hostNC.Get(N_HOSTS-1));

	appSrv.Start(Seconds(1.0));
	appSrv.Stop(Seconds(10.0));

	//The Clients
	for (int i=0; i<N_HOSTS-1; i++) {
		UdpEchoClientHelper echoCli(hIf.GetAddress(N_HOSTS-1), 9); // server's interface
		echoCli.SetAttribute ("MaxPackets", UintegerValue (1000000));
		echoCli.SetAttribute ("Interval", TimeValue (MicroSeconds (100)));
		echoCli.SetAttribute ("PacketSize", UintegerValue (1482));

		ApplicationContainer appCli = echoCli.Install(hostNC.Get(i));

		appCli.Start(Seconds(2.0));
		appCli.Stop(Seconds(10.0));
	}
#endif

//The Animation
//------------
	for (int i=0; i<N_HOSTS-1; i++)
		AnimationInterface::SetConstantPosition(hostNC.Get(i), 10.0, 10.0+i*10.0, 0.0);

	AnimationInterface::SetConstantPosition(hostNC.Get(N_HOSTS-1), 20.0+N_HOSTS*10.0, N_HOSTS*10.0/2, 0.0);
	AnimationInterface::SetConstantPosition(swNC.Get(0)  , 20.0+N_HOSTS/2*10.0, N_HOSTS*10.0/2, 0.0);

	AnimationInterface anim ("hh_sw_h_of_topo.xml");

	Simulator::Run ();
	Simulator::Destroy ();

	return 0;
}
