
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
#include "ns3/log.h"
#include "ns3/netanim-module.h"
#include "ns3/net-device.h"
#include "ns3/bridge-helper.h"
#include "ns3/bridge-net-device.h"
#include "ns3/csma-module.h"
#include "ns3/csma-net-device.h"
#include "ns3/queue.h"
#include "ns3/qcn-cp-queue.h"
#include "ns3/qcn-rp-queue.h"
#include "ns3/qcn-fb-proto.h"
#include "ns3/callback.h"

using namespace ns3;
using namespace std;

NS_LOG_COMPONENT_DEFINE ("TreeCongestionTopology");

#define SW_CORE_N	1	// Number of Core SWes
#define SW_AGG_N	2	// Number of Aggregation SWes
#define SW_EDGE_N	4	// Number of Edge SWes
#define HOST_N		8	// Number of Hosts

#define CH_C_TO_A SW_AGG_N	// Links from core to aggregation
#define CH_A_TO_E SW_EDGE_N	// Links from aggregation to edge
#define CH_E_TO_H HOST_N	// Links from edge to hosts

#define DEV_PER_SW 3		// Each sw has 3 devices: an upling and 2 downlinks
#define DEV_PER_CORE_SW 2	// The Core SW has only 2 ports

#define CABLE_DELAY 250		// UTP & Fiber Optics cable delay=5ns/m -> 50m=250ns
#define CABLE_DELAY_C_TO_A 500	// UTP & FO delay=5ns/m -> 100m=500ns
#define CABLE_DELAY_A_TO_E 75	// UTP & FO delay=5ns/m -> 15m=75ns
#define CABLE_DELAY_E_TO_H 10	// UTP & FO delay=5ns/m -> 2m=10ns
#define LINE_RATE "1Gbps"	// 1Gbps each connection line

#define DEV_MTU 1500		// 1500Bytes per MTU
#define QUEUE_LEN 1500*100	// 100 frames of 1500Bytes (MTU)

#define APP_ACTIVE
#define QCN_ACTIVE

// The Tree topology
//=================================================================================================================
//
//                                      +---------------+ Core
//                                      |     Node0     |
//                                      |          CP   |
//                                      +--Dev0----Dev1-+
//                       __________________/        \__________________
//                      |         0                          1         |
//              +------Dev0-----+                               +------Dev0-----+ Aggregation
//              |      CP       |                               |     Node1     |
//              |     Node0     |                               |  CP      CP   |
//              +--Dev1----Dev2-+                               +--Dev1----Dev2-+
//           ______|         |______                          ______|         |_______
//          |     0            1    |                        |    2             3     |
//+--------Dev0---------+ +--------Dev0---------+ +--------Dev0----------+ +---------Dev0---------+ Edge
//|        CP           | |        CP           | |       Node2          | |       Node3          |
//|         Node0       | |        Node1        | |   CP          CP     | |   CP          CP     |
//+---Dev1--------Dev2--+ +---Dev1-------Dev2---+ +---Dev1--------Dev2---+ +---Dev1--------Dev2---+ 
//     |           |           |          |            |           |            |           |     
//     |0          |1          |2         |3           |4          |5           |6          |7    
//     |           |           |          |            |           |            |           |     
//     RP          RP          RP         RP           RP          RP           RP          RP      
//     H0          H1          H2         H3           H4          H5           H6          H7       
//     C           C           C                                   S            S           s     <-Active clients
//|______________________+______________________| |_______________________+_______________________|
//                       |                                                |
//                  Clients/RPs                                        Servers
//
//=================================================================================================================

// Step by step initialization:
//=================================================================================================================
// 1. Create Nodes 
// 2. Create Channels
//    2.1. Set link data rate
//    2.2. Set link delay
// 3. Create CP/RP Queues 
//    3.1. Set queues length
// 4. Create Csma Net Devices
//    4.1. Set MTU
//    4.3. Set MAC
//    4.4. Add devices to nodes
//    4.5. Attach queues
// 5. Create Topology
//    5.1. Attach Channels
// 6. Set Active CPs and RPs
// 7. Register QCN callbacks
// 8. Install Bridges on SW nodes
// 9. Install Internet stack and interfaces on host nodes
//=================================================================================================================

int
main (int argc, char *argv[])
{
	Time::SetResolution (Time::NS);

//The Logging
//-----------
//	LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
//	LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);
//	LogComponentEnable("CsmaNetDevice", LOG_LEVEL_INFO);
//	LogComponentEnable("DropTailQueue", LOG_LEVEL_ALL);
//	LogComponentEnable("QCNCPQueue", LOG_LEVEL_ALL);
//	LogComponentEnable("CsmaNetDevice", LOG_LEVEL_ALL);

// Create nodes: Switches and Hosts
//---------------------------------
	NodeContainer swCoreNC;
	NodeContainer swAggNC;
	NodeContainer swEdgeNC;
	swCoreNC.Create(SW_CORE_N);
	swAggNC.Create(SW_AGG_N);
	swEdgeNC.Create(SW_EDGE_N);

	NodeContainer hostNC;
	hostNC.Create(HOST_N);

// Create channels
//----------------
	Ptr<CsmaChannel> chCoreToAgg [CH_C_TO_A];// Channels from core to aggregation swes
	Ptr<CsmaChannel> chAggToEdge [CH_A_TO_E];// Channels from aggregation to edge swes
	Ptr<CsmaChannel> chEdgeToHost[CH_E_TO_H];// Channels from edge swes to hosts
	for (int i=0; i<CH_C_TO_A; i++) {
		chCoreToAgg[i] = CreateObject <CsmaChannel> ();
		chCoreToAgg[i]->SetAttribute ("DataRate", StringValue (LINE_RATE));
		chCoreToAgg[i]->SetAttribute ("Delay", TimeValue (NanoSeconds (CABLE_DELAY_C_TO_A)));
	}
	for (int i=0; i<CH_A_TO_E; i++) {
		chAggToEdge[i] = CreateObject <CsmaChannel> ();
		chAggToEdge[i]->SetAttribute ("DataRate", StringValue (LINE_RATE));
		chAggToEdge[i]->SetAttribute ("Delay", TimeValue (NanoSeconds (CABLE_DELAY_A_TO_E)));
	}
	for (int i=0; i<CH_E_TO_H; i++) {
		chEdgeToHost[i] = CreateObject <CsmaChannel> ();
		chEdgeToHost[i]->SetAttribute ("DataRate", StringValue (LINE_RATE));
		chEdgeToHost[i]->SetAttribute ("Delay", TimeValue (NanoSeconds (CABLE_DELAY_E_TO_H)));
	}
	
// Create queues
//--------------
	Ptr<QCNCPQueue> qCore[SW_CORE_N][DEV_PER_CORE_SW];
	Ptr<QCNCPQueue> qAgg [SW_AGG_N ][DEV_PER_SW];
	Ptr<QCNCPQueue> qEdge[SW_EDGE_N][DEV_PER_SW];
	for (int i=0; i<SW_CORE_N; i++)
		for (int j=0; j<DEV_PER_CORE_SW; j++) {
			qCore[i][j] = CreateObject <QCNCPQueue> ();
			qCore[i][j]->SetAttribute("MaxBytes", UintegerValue (QUEUE_LEN));
			qCore[i][j]->ActiveCP(false);
		}
	for (int i=0; i<SW_AGG_N; i++)
		for (int j=0; j<DEV_PER_SW; j++) {
			qAgg[i][j] = CreateObject <QCNCPQueue> ();
			qAgg[i][j]->SetAttribute("MaxBytes", UintegerValue (QUEUE_LEN));
			qAgg[i][j]->ActiveCP(false);
		}
	for (int i=0; i<SW_EDGE_N; i++)
		for (int j=0; j<DEV_PER_SW; j++) {
			qEdge[i][j] = CreateObject <QCNCPQueue> ();
			qEdge[i][j]->SetAttribute("MaxBytes", UintegerValue (QUEUE_LEN));
			qEdge[i][j]->ActiveCP(false);
		}

	Ptr<QCNRPQueue> qHost[HOST_N];
	for (int i=0; i<HOST_N; i++) {
		qHost[i] = CreateObject <QCNRPQueue> ();
		qHost[i]->SetAttribute("MaxBytes", UintegerValue (QUEUE_LEN));
		qHost[i]->ActiveRP(false);
	}

// Create CSMA devices
//--------------------
	Ptr<CsmaNetDevice> csmaNetDevCore[SW_CORE_N][DEV_PER_CORE_SW];
	Ptr<CsmaNetDevice> csmaNetDevAgg [SW_AGG_N ][DEV_PER_SW];
	Ptr<CsmaNetDevice> csmaNetDevEdge[SW_EDGE_N][DEV_PER_SW];
	for (int i=0; i<SW_CORE_N; i++)
		for (int j=0; j<DEV_PER_CORE_SW; j++) {
			csmaNetDevCore[i][j] = CreateObject <CsmaNetDevice> ();
			csmaNetDevCore[i][j]->SetAttribute("Mtu", UintegerValue (DEV_MTU));
			csmaNetDevCore[i][j]->SetAddress (Mac48Address::Allocate ());
		}
	for (int i=0; i<SW_AGG_N; i++)
		for (int j=0; j<DEV_PER_SW; j++) {
			csmaNetDevAgg[i][j] = CreateObject <CsmaNetDevice> ();
			csmaNetDevAgg[i][j]->SetAttribute("Mtu", UintegerValue (DEV_MTU));
			csmaNetDevAgg[i][j]->SetAddress (Mac48Address::Allocate ());
		}
	for (int i=0; i<SW_EDGE_N; i++)
		for (int j=0; j<DEV_PER_SW; j++) {
			csmaNetDevEdge[i][j] = CreateObject <CsmaNetDevice> ();
			csmaNetDevEdge[i][j]->SetAttribute("Mtu", UintegerValue (DEV_MTU));
			csmaNetDevEdge[i][j]->SetAddress (Mac48Address::Allocate ());
		}

	Ptr<CsmaNetDevice> csmaNetDevHost[HOST_N];
	for (int i=0; i<HOST_N; i++) {
		csmaNetDevHost[i] = CreateObject <CsmaNetDevice> ();
		csmaNetDevHost[i]->SetAttribute("Mtu", UintegerValue (DEV_MTU));
		csmaNetDevHost[i]->SetAddress (Mac48Address::Allocate ());
	}

// Add Csma Net Devs to nodes
//---------------------------
	for (int i=0; i<SW_CORE_N; i++) {
		Ptr<Node> node = swCoreNC.Get(i);
		for (int j=0; j<DEV_PER_CORE_SW ;j++)
			node->AddDevice (csmaNetDevCore[i][j]);
	}
	for (int i=0; i<SW_AGG_N; i++) {
		Ptr<Node> node = swAggNC.Get(i);
		for (int j=0; j<DEV_PER_SW ;j++)
			node->AddDevice (csmaNetDevAgg[i][j]);
	}
	for (int i=0; i<SW_EDGE_N; i++) {
		Ptr<Node> node = swEdgeNC.Get(i);
		for (int j=0; j<DEV_PER_SW ;j++)
			node->AddDevice (csmaNetDevEdge[i][j]);
	}
	for (int i=0; i<HOST_N; i++) {
		Ptr<Node> node = hostNC.Get(i);
		node->AddDevice (csmaNetDevHost[i]);
	}

// Attach queues to Csma Net devs
//-------------------------------
	for (int i=0; i<SW_CORE_N; i++)
		for (int j=0; j<DEV_PER_CORE_SW ;j++) {
			csmaNetDevCore[i][j]->SetQueue (qCore[i][j]);
			Ptr<NetDevice> netDev = csmaNetDevCore[i][j]->GetObject<NetDevice> ();
			qCore[i][j]->SetNetDev(netDev);
		}
	for (int i=0; i<SW_AGG_N; i++)
		for (int j=0; j<DEV_PER_SW ;j++){
			csmaNetDevAgg[i][j]->SetQueue (qAgg[i][j]);
			Ptr<NetDevice> netDev = csmaNetDevAgg[i][j]->GetObject<NetDevice> ();
			qAgg[i][j]->SetNetDev(netDev);
		}
	for (int i=0; i<SW_EDGE_N; i++)
		for (int j=0; j<DEV_PER_SW ;j++) {
			csmaNetDevEdge[i][j]->SetQueue (qEdge[i][j]);
			Ptr<NetDevice> netDev = csmaNetDevEdge[i][j]->GetObject<NetDevice> ();
			qEdge[i][j]->SetNetDev(netDev);
		}
	for (int i=0; i<HOST_N; i++){
		csmaNetDevHost[i]->SetQueue (qHost[i]);
		Ptr<NetDevice> netDev = csmaNetDevHost[i]->GetObject<NetDevice> ();
		qHost[i]->SetNetDev(netDev);
	}

// Create topology
//----------------
	// Connect Core SW's csma net-devs to Aggregation SW's csma net-devs
	chCoreToAgg[0]->Attach(csmaNetDevCore[0][0]);
	chCoreToAgg[0]->Attach(csmaNetDevAgg [0][0]);
	csmaNetDevCore[0][0]->Attach(chCoreToAgg[0]);
	csmaNetDevAgg [0][0]->Attach(chCoreToAgg[0]);

	chCoreToAgg[1]->Attach(csmaNetDevCore[0][1]);
	chCoreToAgg[1]->Attach(csmaNetDevAgg [1][0]);
	csmaNetDevCore[0][1]->Attach(chCoreToAgg[1]);
	csmaNetDevAgg [1][0]->Attach(chCoreToAgg[1]);

	// Connect Aggregation SW's csma net-devs to Edge SW's csma net-devs
	chAggToEdge[0]->Attach(csmaNetDevAgg [0][1]);
	chAggToEdge[0]->Attach(csmaNetDevEdge[0][0]);
	csmaNetDevAgg [0][1]->Attach(chAggToEdge[0]);
	csmaNetDevEdge[0][0]->Attach(chAggToEdge[0]);
	
	chAggToEdge[1]->Attach(csmaNetDevAgg [0][2]);
	chAggToEdge[1]->Attach(csmaNetDevEdge[1][0]);
	csmaNetDevAgg [0][2]->Attach(chAggToEdge[1]);
	csmaNetDevEdge[1][0]->Attach(chAggToEdge[1]);

	chAggToEdge[2]->Attach(csmaNetDevAgg [1][1]);
	chAggToEdge[2]->Attach(csmaNetDevEdge[2][0]);
	csmaNetDevAgg [1][1]->Attach(chAggToEdge[2]);
	csmaNetDevEdge[2][0]->Attach(chAggToEdge[2]);

	chAggToEdge[3]->Attach(csmaNetDevAgg [1][2]);
	chAggToEdge[3]->Attach(csmaNetDevEdge[3][0]);
	csmaNetDevAgg [1][2]->Attach(chAggToEdge[3]);
	csmaNetDevEdge[3][0]->Attach(chAggToEdge[3]);

	// Connect Host's csma net-devs to Edge SW's csma net-devs
	chEdgeToHost[0]->Attach(csmaNetDevEdge[0][1]);
	chEdgeToHost[1]->Attach(csmaNetDevEdge[0][2]);
	chEdgeToHost[2]->Attach(csmaNetDevEdge[1][1]);
	chEdgeToHost[3]->Attach(csmaNetDevEdge[1][2]);
	chEdgeToHost[4]->Attach(csmaNetDevEdge[2][1]);
	chEdgeToHost[5]->Attach(csmaNetDevEdge[2][2]);
	chEdgeToHost[6]->Attach(csmaNetDevEdge[3][1]);
	chEdgeToHost[7]->Attach(csmaNetDevEdge[3][2]);
	csmaNetDevEdge[0][1]->Attach(chEdgeToHost[0]);
	csmaNetDevEdge[0][2]->Attach(chEdgeToHost[1]);
	csmaNetDevEdge[1][1]->Attach(chEdgeToHost[2]);
	csmaNetDevEdge[1][2]->Attach(chEdgeToHost[3]);
	csmaNetDevEdge[2][1]->Attach(chEdgeToHost[4]);
	csmaNetDevEdge[2][2]->Attach(chEdgeToHost[5]);
	csmaNetDevEdge[3][1]->Attach(chEdgeToHost[6]);
	csmaNetDevEdge[3][2]->Attach(chEdgeToHost[7]);

	chEdgeToHost[0]->Attach(csmaNetDevHost[0]);
	chEdgeToHost[1]->Attach(csmaNetDevHost[1]);
	chEdgeToHost[2]->Attach(csmaNetDevHost[2]);
	chEdgeToHost[3]->Attach(csmaNetDevHost[3]);
	chEdgeToHost[4]->Attach(csmaNetDevHost[4]);
	chEdgeToHost[5]->Attach(csmaNetDevHost[5]);
	chEdgeToHost[6]->Attach(csmaNetDevHost[6]);
	chEdgeToHost[7]->Attach(csmaNetDevHost[7]);
	csmaNetDevHost[0]->Attach(chEdgeToHost[0]);
	csmaNetDevHost[1]->Attach(chEdgeToHost[1]);
	csmaNetDevHost[2]->Attach(chEdgeToHost[2]);
	csmaNetDevHost[3]->Attach(chEdgeToHost[3]);
	csmaNetDevHost[4]->Attach(chEdgeToHost[4]);
	csmaNetDevHost[5]->Attach(chEdgeToHost[5]);
	csmaNetDevHost[6]->Attach(chEdgeToHost[6]);
	csmaNetDevHost[7]->Attach(chEdgeToHost[7]);

#if defined(QCN_ACTIVE)
// Set active CPs and RPs according to picture above
// Register QCN callbacks
//--------------------------------------------------
	qCore[0][1]->ActiveCP(true);

	qAgg[0][0]->ActiveCP(true);
	qAgg[1][1]->ActiveCP(true);
	qAgg[1][2]->ActiveCP(true);

	qEdge[0][0]->ActiveCP(true);
	qEdge[1][0]->ActiveCP(true);
	qEdge[2][1]->ActiveCP(true);
	qEdge[2][2]->ActiveCP(true);
	qEdge[3][1]->ActiveCP(true);
	qEdge[3][2]->ActiveCP(true);

	qHost[0]->ActiveRP(true);
		qHost[0]->GetNetDev()->GetNode()->RegisterProtocolHandler(
				MakeCallback(&ns3::QCNRPQueue::Rcv_Fbfrm, qHost[0]),
				(uint16_t)QCN_PROTO_NO,
				qHost[0]->GetNetDev(),
				false);
	qHost[1]->ActiveRP(true);
		qHost[1]->GetNetDev()->GetNode()->RegisterProtocolHandler(
				MakeCallback(&ns3::QCNRPQueue::Rcv_Fbfrm, qHost[1]),
				(uint16_t)QCN_PROTO_NO,
				qHost[1]->GetNetDev(),
				false);
	qHost[2]->ActiveRP(true);
		qHost[2]->GetNetDev()->GetNode()->RegisterProtocolHandler(
				MakeCallback(&ns3::QCNRPQueue::Rcv_Fbfrm, qHost[2]),
				(uint16_t)QCN_PROTO_NO,
				qHost[2]->GetNetDev(),
				false);
#endif

//Install Bridges on SWes
//-----------------------
	BridgeHelper swBridgeHelper;
	// A bridge device per SW
	NetDeviceContainer swCoreBridgePorts[SW_CORE_N];
	NetDeviceContainer swAggBridgePorts [SW_AGG_N];
	NetDeviceContainer swEdgeBridgePorts[SW_EDGE_N];

	for (int i=0; i<SW_CORE_N; i++ ) {
		for (int j=0; j<DEV_PER_CORE_SW ;j++) {
			Ptr<NetDevice> netDev = csmaNetDevCore[i][j]->GetObject<NetDevice> ();
			swCoreBridgePorts[i].Add(netDev);
		}
		swBridgeHelper.Install(swCoreNC.Get(i), swCoreBridgePorts[i]);
	}
	for (int i=0; i<SW_AGG_N; i++ ) {
		for (int j=0; j<DEV_PER_SW ;j++) {
			Ptr<NetDevice> netDev = csmaNetDevAgg[i][j]->GetObject<NetDevice> ();
			swAggBridgePorts[i].Add(netDev);
		}
		swBridgeHelper.Install(swAggNC.Get(i), swAggBridgePorts[i]);
	}
	for (int i=0; i<SW_EDGE_N; i++ ) {
		for (int j=0; j<DEV_PER_SW ;j++) {
			Ptr<NetDevice> netDev = csmaNetDevEdge[i][j]->GetObject<NetDevice> ();
			swEdgeBridgePorts[i].Add(netDev);
		}
		swBridgeHelper.Install(swEdgeNC.Get(i), swEdgeBridgePorts[i]);
	}

//The Internet Stack
//------------------
	InternetStackHelper internet;
	internet.Install(hostNC);

	Ipv4AddressHelper addrBase;
	addrBase.SetBase("10.1.1.0", "255.255.255.0");
	Ipv4InterfaceContainer itf;

	for (int i=0; i<HOST_N; i++){
		Ptr<NetDevice> netDev = csmaNetDevHost[i]->GetObject<NetDevice> ();
		itf.Add(addrBase.Assign(netDev));
	}

//The Applications
//----------------
#if defined(APP_ACTIVE)
	UdpEchoServerHelper echoSrv(9);
	ApplicationContainer appSrv5 = echoSrv.Install(hostNC.Get(5));
//	ApplicationContainer appSrv6 = echoSrv.Install(hostNC.Get(6));
//	ApplicationContainer appSrv7 = echoSrv.Install(hostNC.Get(7));

    PcapHelper pcapHelper;
    Ptr<PcapFileWrapper> file = pcapHelper.CreateFile ("../log/qcn.pcap", std::ios::out,
                                                       PcapHelper::DLT_EN10MB);
    pcapHelper.HookDefaultSink<CsmaNetDevice> (csmaNetDevHost[5]->GetObject<CsmaNetDevice> (), "PromiscSniffer", file);



	appSrv5.Start(Seconds(1.0));
	appSrv5.Stop(Seconds(5.0)); //10
//	appSrv6.Start(Seconds(1.0));
//	appSrv6.Stop(Seconds(10.0));
//	appSrv7.Start(Seconds(1.0));
//	appSrv7.Stop(Seconds(10.0));

	UdpEchoClientHelper echoCli0(itf.GetAddress(5), 9); // server's interface
//	UdpEchoClientHelper echoCli1(itf.GetAddress(6), 9); // server's interface
//	UdpEchoClientHelper echoCli2(itf.GetAddress(7), 9); // server's interface
	echoCli0.SetAttribute ("MaxPackets", UintegerValue (10)); //10000
	echoCli0.SetAttribute ("Interval", TimeValue (MicroSeconds (1000))); //100
	echoCli0.SetAttribute ("PacketSize", UintegerValue (1482));
//	echoCli1.SetAttribute ("MaxPackets", UintegerValue (10000));
//	echoCli1.SetAttribute ("Interval", TimeValue (MicroSeconds (100)));
//	echoCli1.SetAttribute ("PacketSize", UintegerValue (1482));
//	echoCli2.SetAttribute ("MaxPackets", UintegerValue (10000));
//	echoCli2.SetAttribute ("Interval", TimeValue (MicroSeconds (100)));
//	echoCli2.SetAttribute ("PacketSize", UintegerValue (1482));
	ApplicationContainer appCli0 = echoCli0.Install(hostNC.Get(0));
//	ApplicationContainer appCli1 = echoCli1.Install(hostNC.Get(1));
//	ApplicationContainer appCli2 = echoCli2.Install(hostNC.Get(2));

	appCli0.Start(Seconds(2.0));
	appCli0.Stop(Seconds(5.0)); //10
//	appCli1.Start(Seconds(2.0));
//	appCli1.Stop(Seconds(10.0));
//	appCli2.Start(Seconds(2.0));
//	appCli2.Stop(Seconds(10.0));
#endif


//The Animation
//------------
	AnimationInterface::SetConstantPosition(swCoreNC.Get(0), 450.0, 10.0, 0.0);

	AnimationInterface::SetConstantPosition(swAggNC.Get(0) , 250.0, 100.0, 0.0);
	AnimationInterface::SetConstantPosition(swAggNC.Get(1) , 650.0, 100.0, 0.0);

	AnimationInterface::SetConstantPosition(swEdgeNC.Get(0), 150.0, 200.0, 0.0);
	AnimationInterface::SetConstantPosition(swEdgeNC.Get(1), 350.0, 200.0, 0.0);
	AnimationInterface::SetConstantPosition(swEdgeNC.Get(2), 550.0, 200.0, 0.0);
	AnimationInterface::SetConstantPosition(swEdgeNC.Get(3), 750.0, 200.0, 0.0);

	AnimationInterface::SetConstantPosition(hostNC.Get(0)  , 75.0 , 300.0, 0.0);
	AnimationInterface::SetConstantPosition(hostNC.Get(1)  , 225.0, 300.0, 0.0);
	AnimationInterface::SetConstantPosition(hostNC.Get(2)  , 275.0, 300.0, 0.0);
	AnimationInterface::SetConstantPosition(hostNC.Get(3)  , 425.0, 300.0, 0.0);
	AnimationInterface::SetConstantPosition(hostNC.Get(4)  , 475.0, 300.0, 0.0);
	AnimationInterface::SetConstantPosition(hostNC.Get(5)  , 625.0, 300.0, 0.0);
	AnimationInterface::SetConstantPosition(hostNC.Get(6)  , 675.0, 300.0, 0.0);
	AnimationInterface::SetConstantPosition(hostNC.Get(7)  , 825.0, 300.0, 0.0);

	AnimationInterface anim ("../log/qcn_tree_topo.xml");

//Start emulation
//---------------
	Simulator::Run ();
	Simulator::Destroy ();

	return 0;
}
