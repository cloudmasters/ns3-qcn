/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

#include <stdint.h>

#include "ns3/core-module.h"
#include "ns3/applications-module.h"
#include "ns3/netanim-module.h"

#include "ns3/csma-module.h"
#include "ns3/csma-helper.h"

#include "ns3/topology-module.h"

using namespace ns3;

// ===========================================================================
//
//        client                 server
//   +----------------+    +----------------+
//   |      TCP       |    |      TCP       |
//   +----------------+    +----------------+
//   |    10.1.1.1    |    |    10.1.1.2    |
//   +----------------+    +----------------+
//   |      csma      |    |      csma      |
//   +----------------+    +----------------+
//           |                     |
//           +---------------------+
//                    channel
//
// ===========================================================================

#define CABLE_DELAY 10     // UTP & Fiber Optics cable delay=5ns/m -> 2m=10ns
#define LINE_RATE "1Gbps"   // 1Gbps each connection line

NS_LOG_COMPONENT_DEFINE ("SimpleGuest2Guest");

int 
main (int argc, char *argv[])
{
    LogComponentEnable ("SimpleGuest2Guest", LOG_LEVEL_ALL);
    Time::SetResolution (Time::NS);
    GlobalValue::Bind ("ChecksumEnabled", BooleanValue (true));

    //Create IP subnet allocator
    Ipv4AddressHelper addrBase;
    addrBase.SetBase("10.1.1.0", "255.255.255.0");

    //Create two guests
    Guest client(addrBase.NewAddress(),"255.255.255.0",false);
    Guest server(addrBase.NewAddress(),"255.255.255.0",false);
    client.setPosition(0,0,0);
    server.setPosition(100,0,0);

    //Create channel between the guests
    Ptr<CsmaChannel> channel = CreateObject<CsmaChannel>();
    channel->SetAttribute ("DataRate", StringValue (LINE_RATE));
    channel->SetAttribute ("Delay", TimeValue (NanoSeconds (CABLE_DELAY)));
    client.Attach(channel);
    server.Attach(channel);

    //Create application
    UdpEchoServerHelper echoServer(1028);
    ApplicationContainer appServer = echoServer.Install(server.GetNode());

    UdpEchoClientHelper echoClient(server.GetIpv4Address(),1028);
    ApplicationContainer appClient=echoClient.Install(client.GetNode());

    echoClient.SetAttribute ("MaxPackets", UintegerValue (2));
    echoClient.SetAttribute ("Interval", TimeValue (Seconds(1)));
    echoClient.SetAttribute ("PacketSize", UintegerValue (1000));

 //   Ipv4StaticRoutingHelper ipv4RoutingHelper;
    // Create static routes from A to C
//   Ptr<Ipv4StaticRouting> staticRouting = ipv4RoutingHelper.GetStaticRouting(client.GetNode()->GetObject<Ipv4>());
 //   Ptr<OutputStreamWrapper> routingtable = Create<OutputStreamWrapper>("../log/routingtable",std::ios::out);
 //   ipv4RoutingHelper.PrintRoutingTableAt(Seconds(1),client.GetNode(),routingtable);

    Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

    appServer.Start(Seconds(0));
    appClient.Start(Seconds(1));
    appClient.Stop(Seconds(20));
    appServer.Stop(Seconds(30));

    //Pcap
    CsmaHelper clientpcap;
//    clientpcap.EnablePcapAll("../log/client-all.pcap");
//    clientpcap.EnableAscii(routingtable,client.GetNetDevice());
    clientpcap.EnablePcap("../log/client.pcap",client.GetNetDevice(),false,true);
    CsmaHelper serverpcap;
    serverpcap.EnablePcap("../log/server.pcap",server.GetNetDevice());
//    serverpcap.EnableAscii(routingtable,server.GetNetDevice());


     PcapHelper pcapHelper;
     Ptr<PcapFileWrapper> file = pcapHelper.CreateFile ("../log/client-drop.pcap", std::ios::out,
                                                        PcapHelper::DLT_EN10MB);
     pcapHelper.HookDefaultSink<CsmaNetDevice> (client.GetNetDevice()->GetObject<CsmaNetDevice>(), "MacTx", file);

     NS_LOG_INFO("No devices: " << channel->GetNDevices() );
     for(uint32_t i =0; i< channel->GetNDevices(); i++)
     {
         NS_LOG_INFO("Device:" << channel->GetDevice(i));
     }


    AnimationInterface anim ("../log/guest2guest.xml");

    Simulator::Run ();
    Simulator::Destroy ();
    return 0;
}


