/*
 * tpo_host.h
 *
 *      Author: ovidiu
 */

#ifndef TPO_HOST_H_
#define TPO_HOST_H_

#include "ns3/core-module.h"
#include "ns3/object.h"
#include "ns3/node.h"
#include "ns3/application.h"
#include "ns3/ipv4-address.h"
#include "ns3/csma-net-device.h"
#include "ns3/csma-channel.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/ipv4-address.h"
#include "ns3/animation-interface.h"
#include "ns3/openflow-module.h"

#include "ns3/qcn-rp-queue.h"

#include "tpo_guest.h"

using namespace ns3::ofi;

namespace ns3 {

class Host {
public:
	Host();
	Host(uint32_t ports, uint32_t guests, Ptr<LearningController> m_controller)
	virtual ~Host();
	Init();//create the switch and add the controller to it

	CreateNGuests(uint32_t i);
	Ptr<Guest> CreateGuest();
	uint32_t AddGuest(Ptr<Guest>);
	uint32_t GetNGuests();
	Ptr<Guest> GetGuest(uint32_t i);

	Ptr<CsmaNetDevice> GetDataInterface(uint32_t i);
	Ptr<CsmaNetDevice> GetGuestInterface(uint32_t i);
	uint32_t GetNDataInterfaces();
	uint32_t GetNGuestInterfaces();
	Attach(Ptr<CsmaChannel> channel);
	Attach(Ptr<CsmaChannel> channel,uint32_t ifindex);

private:
	//TODO: finalize it
	std::list<Guest> guest_list;
    uint32_t m_guests;//number of guests
	uint32_t m_guestifs;//number of guest ports
	uint32_t m_dataifs;//number of data ports
	Ptr<LearningController> m_controller; //openflow controller

};

} /* namespace ns3 */

#endif /* TPO_HOST_H_ */
