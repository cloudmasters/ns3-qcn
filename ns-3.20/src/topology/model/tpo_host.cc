/*
 * tpo_host.cc
 *
 *      Author: ovidiu
 */

#include "tpo_host.h"

namespace ns3 {

// A host is composed of a single switch (possibly with multiple
// internal bridges) and multiple guests

    // Scratch zone:
    /*=================================================================================================================
     * Input
     * - Controller Object
     * - Number of guests
     * - Guest IP Class
     * Setters:
     * - Relative position in animation
     * Getters:
     * - getGuest(int no)
     * - getBridge()
     *=================================================================================================================*/

    // Step by step initialization:
    /*=================================================================================================================
     * 1. Create nodes for host & bridge with correct port number
     *    1.1 Create host node
     *    1.2 Create bridge node
     *    1.3 Create guests - without position
     * 2. Connect the guest to the switch
     * 3. Set position in animation
     *     3.1 Set position for self nodes
     *     3.2 Set position for guests
     *=================================================================================================================*/

Host()
{

}

Host::Init()
{

}

Host::CreateNGuests(uint32_t i)
{

}

Ptr<Guest>
Host::CreateGuest()
{
return 0;
}

uint32_t
Host::AddGuest(Ptr<Guest>)
{
    return 0;
}

uint32_t
Host::GetNGuests()
{
    return m_guests;
}

Ptr<Guest>
Host::GetGuest(uint32_t i)
{

}

Ptr<CsmaNetDevice>
Host::GetDataInterface(uint32_t i)
{

}

Ptr<CsmaNetDevice>
Host::GetGuestInterface(uint32_t i)
{

}

uint32_t
Host::GetNDataInterfaces()
{
    return 0;
}

uint32_t
Host::GetNGuestInterfaces()
{
    return 0;
}

Host::Attach(Ptr<CsmaChannel> channel)
{

}

Host::Attach(Ptr<CsmaChannel> channel,uint32_t ifindex)
{

}

Host::~Host() {

}

} /* namespace ns3 */
