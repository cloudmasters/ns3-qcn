/*
 * tpo_switch.h
 *
 *      Author: ovidiu
 */

#ifndef TPO_SWITCH_H_
#define TPO_SWITCH_H_

namespace ns3 {

class Switch {
public:
	Switch();
	virtual ~Switch();
};

} /* namespace ns3 */

#endif /* TPO_SWITCH_H_ */
