/*
 * tpo_guest.h
 *
 *      Author: ovidiu
 */

#ifndef TPO_GUEST_H_
#define TPO_GUEST_H_

#include <stdint.h>

#include "ns3/core-module.h"
#include "ns3/object.h"
#include "ns3/node.h"
#include "ns3/application.h"
#include "ns3/ipv4-address.h"
#include "ns3/csma-net-device.h"
#include "ns3/csma-channel.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/ipv4-address.h"
#include "ns3/animation-interface.h"

#include "ns3/qcn-rp-queue.h"

namespace ns3
{

  class Guest : public Object
  {
  public:
    static TypeId GetTypeId (void);

    Guest();
    Guest(Ipv4Address address, Ipv4Mask mask, bool statusRP);
    Guest(Ipv4Address address, Ipv4Mask mask, bool statusRP, Ptr<CsmaChannel> channel);

    Ptr<Node> GetNode();
    Ptr<NetDevice> GetNetDevice();
    Ipv4Address GetIpv4Address();
    Ipv4Mask GetIpv4Mask();
    void SetIpv4Address(Ipv4Address address, Ipv4Mask mask);
    void SetActiveRP(bool statusRP);
    void Attach(Ptr<CsmaChannel> channel);

    //Position
    void setPosition(double x, double y, double z);

    virtual ~Guest();
  private:
    Ptr<Node> m_node;
    Ptr<CsmaNetDevice> m_virtualIf;
    Ptr<Queue> m_queue;
    Ipv4Address m_ipv4address;
    Ipv4Mask m_ipv4mask;
    void Init();
  };

} /* namespace ns3 */

#endif /* TPO_GUEST_H_ */
