/*
 * tpo_guest.cc
 *
 *      Author: ovidiu
 */



#include "ns3/tpo_guest.h"


/* Supported applications:
 * UDP:
 *  1. Server
 * 	2. Random packet with MaxPackets, Interval & PacketSize
 * 	3. IMIX Pcap file of choice
 * TCP:
 *  1. Server
 * 	2. Client or server stream of requested size or start/stop flow
 */

//TODO: Create attributes
#define DEFAULT_QUEUE_LEN   1500*100  // 100 frames of 1500Bytes (MTU)
#define DEFAULT_RP_ACTIVE   false
#define DEFAULT_APP_ACTIVE  true
#define DEV_MTU 1500


namespace ns3 {

    NS_OBJECT_ENSURE_REGISTERED (Guest);

    TypeId Guest::GetTypeId (void)
    {
        static TypeId tid = TypeId ("ns3::Guest")
                    .SetParent<Object> ()
                    ;
        return tid;
    }

    Guest::Guest()
    {
        Init();
    }

    Guest::Guest(Ipv4Address address, Ipv4Mask mask, bool statusRP)
    {
        Init();
        SetIpv4Address(address,mask);
        SetActiveRP(statusRP);
    }

    Guest::Guest(Ipv4Address address, Ipv4Mask mask, bool statusRP, Ptr<CsmaChannel> channel)
    {
        Init();
        SetIpv4Address(address,mask);
        SetActiveRP(statusRP);
        Attach(channel);
    }

    void
    Guest::Init()
    {
        m_node=CreateObject<Node> ();

        m_virtualIf=CreateObject<CsmaNetDevice> ();
        m_virtualIf->SetAttribute("Mtu", UintegerValue (DEV_MTU));
        m_virtualIf->SetAddress (Mac48Address::Allocate ());
        m_node->AddDevice(m_virtualIf);

//TODO: RP Queue does not work with pcap traces, need to take a look after the topo is ready
#if 0
        m_queue=CreateObject<QCNRPQueue> ();
        m_queue->SetAttribute("MaxBytes", UintegerValue (DEFAULT_QUEUE_LEN));
        m_queue->ActiveRP(DEFAULT_RP_ACTIVE);
        m_queue->SetNetDev(m_virtualIf);
        m_virtualIf->SetQueue(m_queue);
#endif
        m_queue=CreateObject<DropTailQueue>();
        m_queue->SetAttribute("MaxBytes", UintegerValue (DEFAULT_QUEUE_LEN));
        m_virtualIf->SetQueue(m_queue);

        //Install Internet stack
        InternetStackHelper internet;
        internet.Install(m_node);
    }

    Ptr<Node>
    Guest::GetNode() {
        return m_node;
    }

    Ptr<NetDevice>
    Guest::GetNetDevice()
    {
        return m_virtualIf;
    }

    Ipv4Address
    Guest::GetIpv4Address()
    {
        return m_ipv4address;
    }

    Ipv4Mask
    Guest::GetIpv4Mask()
    {
        return m_ipv4mask;
    }

    void
    Guest::SetIpv4Address(Ipv4Address address, Ipv4Mask mask)
    {
        Ptr<NetDevice> device = m_virtualIf->GetObject<NetDevice>();

        Ptr<Ipv4> ipv4 = m_node->GetObject<Ipv4> ();
        NS_ASSERT_MSG (ipv4, "Ipv4AddressHelper::Assign(): NetDevice is associated"
                       " with a node without IPv4 stack installed -> fail "
                       "(maybe need to use InternetStackHelper?)");

        int32_t interface = ipv4->GetInterfaceForDevice (m_virtualIf);
        if (interface == -1)
        {
            interface = ipv4->AddInterface (m_virtualIf);
        }
        NS_ASSERT_MSG (interface >= 0, "Ipv4AddressHelper::Assign(): "
                       "Interface index not found");

        Ipv4InterfaceAddress ipv4Addr = Ipv4InterfaceAddress (address, mask);
        ipv4->AddAddress (interface, ipv4Addr);
        ipv4->SetMetric (interface, 1);
        ipv4->SetUp (interface);
        m_ipv4address=address;
        m_ipv4mask=mask;
    }

    void
    Guest::SetActiveRP(bool statusRP)
    {
        //m_queue->ActiveRP(statusRP); //TODO: remove comment when RP works with pcap trace
    }

    void
    Guest::Attach(Ptr<CsmaChannel> channel)
    {
        // Only the interface needs to be attached to a channel,
        // the channel will attach itself to the interface (no need to do it)
        m_virtualIf->Attach(channel);
    }

    void
    Guest::setPosition(double x, double y, double z)
    {
        AnimationInterface::SetConstantPosition(m_node,x,y,z);
    }

    Guest::~Guest()
    {

    }

} /* namespace ns3 */
