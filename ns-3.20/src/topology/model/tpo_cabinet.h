/*
 * tpo_cabinet.h
 *
 *      Author: ovidiu
 */

#ifndef TPO_CABINET_H_
#define TPO_CABINET_H_

namespace ns3 {

class Cabinet {
public:
	Cabinet();
	virtual ~Cabinet();
};

} /* namespace ns3 */

#endif /* TPO_CABINET_H_ */
