#include "ns3/log.h"
#include "ns3/enum.h"
#include "ns3/uinteger.h"
#include "ns3/qcn-fb-proto.h"

#include "ns3/node.h"
#include "ns3/backoff.h"
#include "ns3/address.h"
#include "ns3/net-device.h"
#include "ns3/callback.h"
#include "ns3/packet.h"
#include "ns3/traced-callback.h"
#include "ns3/nstime.h"
#include "ns3/data-rate.h"
#include "ns3/ptr.h"
#include "ns3/mac48-address.h"

//--QCN Feedback packet------------------------------------------------------
//
//  Minimum ETH frame size is 64B, minimum data portion is 46B - padding
//
//       48b      48b        16b         8b      2b     6b      46B*8=368
//  | DST MAC | SRC MAC | Type/Length | FlowID | rsv | QCN FB | ETH Padding |
//
//---------------------------------------------------------------------------
//
//--Not supported CN-TAG
//  QCN-FlowID is last 8bits from MAC SRC address
//

NS_LOG_COMPONENT_DEFINE ("QCNFbHeader");

namespace ns3 {

NS_OBJECT_ENSURE_REGISTERED (QCNFbHeader);

QCNFbHeader::QCNFbHeader ()
{
  NS_LOG_FUNCTION (this);
}

QCNFbHeader::~QCNFbHeader ()
{
  NS_LOG_FUNCTION (this);
}

TypeId
QCNFbHeader::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::QCNFbHeader")
    .SetParent<Header> ()
    .AddConstructor<QCNFbHeader> ()
  ;
  return tid;
}

TypeId
QCNFbHeader::GetInstanceTypeId (void) const
{
  NS_LOG_FUNCTION (this);
  return GetTypeId ();
}

void
QCNFbHeader::Print (std::ostream &os) const
{
  NS_LOG_FUNCTION (this);
  // This method is invoked by the packet printing
  // routines to print the content of my header.
  //os << "data=" << m_data << std::endl;
  os << "data=" << m_data;
}

uint32_t
QCNFbHeader::GetSerializedSize (void) const
{
  NS_LOG_FUNCTION (this);
  // we reserve 2 bytes for QCN Fb header.
  return 2;
}

void
QCNFbHeader::Serialize (Buffer::Iterator start) const
{
  NS_LOG_FUNCTION (this);
  // we can serialize two bytes at the start of the buffer.
  // we write them in network byte order.
  start.WriteHtonU16 (m_data);
}

uint32_t
QCNFbHeader::Deserialize (Buffer::Iterator start)
{
  NS_LOG_FUNCTION (this);
  // we can deserialize two bytes from the start of the buffer.
  // we read them in network byte order and store them
  // in host byte order.
  m_data = start.ReadNtohU16 ();

  // we return the number of bytes effectively read.
  return 2;
}

uint16_t 
QCNFbHeader::GetQCNTag (void) const
{
  NS_LOG_FUNCTION (this);
  return m_data;
}

void
QCNFbHeader::SetFlowID(uint8_t flowID)
{
  NS_LOG_FUNCTION (this);
  m_data &= 0xff; //clean 8MSB
  m_data |= (flowID<<8); //set flowID
}

void 
QCNFbHeader::SetQnzFb(uint8_t QnzFb)
{
  NS_LOG_FUNCTION (this);
  m_data &= (0xff<<8); //clean 8LSB
  m_data |= (QnzFb&0x3f); //set flowID
}

uint8_t
QCNFbHeader::GetFlowID(void)
{
  NS_LOG_FUNCTION (this);
  return (uint8_t)(m_data >> 8);
}

uint8_t
QCNFbHeader::GetQnzFb(void)
{
  NS_LOG_FUNCTION (this);
  return (uint8_t)(m_data & 0x3f);
}

} // namespace ns3

