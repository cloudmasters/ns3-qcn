#ifndef QCNRP_Q_H
#define QCNRP_Q_H

#include <queue>
#include "ns3/packet.h"
#include "ns3/queue.h"

#include "ns3/node.h"
#include "ns3/backoff.h"
#include "ns3/address.h"
#include "ns3/net-device.h"
#include "ns3/callback.h"
#include "ns3/packet.h"
#include "ns3/traced-callback.h"
#include "ns3/nstime.h"
#include "ns3/data-rate.h"
#include "ns3/ptr.h"
#include "ns3/mac48-address.h"

#include "ns3/timer.h"

namespace ns3 {

class TraceContainer;

// REACTION POINT -------------------------------------------------------------
#define MTU		1500		// MTU is 1500KB
#define GD		0.0078125	// The control gain parameter: choosen so that
					//   GD*|qnzFbmax| = 1/2, qnzFbmax = 64 -> Gd = 1/128 
#define BYTE_CNT_TH	MTU * 100	// BYTE COUNTER THRESHOLD: 100 frames of 1500B
#define TIMER_TH	10		// TIMER THRESHOLD: timer threshold per each cycle (10ms baseline)
#define FR_CYCLE_TH	5		// FAST RECOVERY THRESHOLD: fast recovery phase has 5 cycles 
#define R_AI		5000000		// RATE: the rate for ACTIVE INCREASE phase (5Mbps base line)
#define R_HAI 		50000000	// RATE: the rate for HYPER ACTIVE INCREASE phase (50Mbps base line)
#define C		1000000000	// LINE RATE: 1Gbps
#define MIN_RATE 	1000000		// MINIMUM TX RATE: minimum rate of a rate limiter, set to 1Mbps.

#define MAX_RL		32		// NOT USED: the maximum number of rate limiters

// RL STATE (ACTIVE or INACTIVE) ----------------------------------------------
#define INACTIVE	0
#define ACTIVE		1

// RL RATE-INCREASE PHASE -----------------------------------------------------
#define RECOVERY_P	1	// RL PHASE - FAST RECOVERY
#define ACTIVE_P	2	// RL PHASE - ACTIVE
#define HYPER_ACTIVE_P	3	// RL PHASE - HYPER-ACTIVE

struct Rate_Limiter {
  int state;          // state of the rate limiter: active or inactive.
  int qlen;           // the TX queue length of the rate limiter queue

  int flowID;         // the flow id that is associated with the rate limiter.

  double CR;          // the current rate of the rate limiter.
  double TR;          // the target rate of the rate limiter.

  // BYTE COUNTER
  int txByteCnt;      // number of bytes left before increasing the state of the byte counter.
  int RLByteCntCycle; // the stage of the byte counter that the rate limiter, is in.

  // TIMER
  Timer RLTimer;      // the timer of the rate limiter
  int RLTimerCycle;   // the stage of the timer that the rate limiter, is in.
};

class QCNRPQueue : public Queue {
public:
  static TypeId GetTypeId (void);

  QCNRPQueue ();
  virtual ~QCNRPQueue();

  void SetNetDev(Ptr<NetDevice> QnetDev);
  Ptr<NetDevice> GetNetDev(void);

  void 
  Rcv_Fbfrm ( Ptr<NetDevice> device, Ptr<const Packet> packet,
              uint16_t protocol, const Address &from,
              const Address &to, NetDevice::PacketType packetType);
  void ActiveRP(bool statusRP);
private:
  virtual bool DoEnqueue (Ptr<Packet> p);
  virtual Ptr<Packet> DoDequeue (void);
  virtual Ptr<const Packet> DoPeek (void) const;

  std::queue<Ptr<Packet> > m_packets; //!< the packets in the queue
  uint32_t m_maxPackets;              //!< max packets in the queue
  uint32_t m_maxBytes;                //!< max bytes in the queue
  uint32_t m_bytesInQueue;            //!< actual bytes in the queue
  QueueMode m_mode;

  // QCN-RATE-LIMITER ---------------------------------------------------------
  bool isRPActive;

  int oldrlSTATE;
  int rlSTATE;

  Ptr<NetDevice> m_qnetdev;
  Timer eventCycleTimer;

  int r_qnzFb;				// received qnz Fb
  int r_flowID;				// received flow ID
  Rate_Limiter RL[MAX_RL];		// Rate Limiters per network device 
					//   (implemented only one)
  int GetFlowID (Ptr<Packet> p);	// Get flowID from received packet
  int GetRLIdx (int flowID);		// Get RL based on flowID 
  void ActivateRL(int rlIdx,int flowID);// activate a rate limiter
  void DeactivateRL (int rlIdx);	// deactivate a rate limiter

  // QCN-REACTION POINT -------------------------------------------------------
  void RL_UPDATE_PHASE(int rlIdx);	// update RL phase and increase throughput
  void RL_BYTE_COUNTER(Ptr<Packet> p);	// RL byte-counter algorithm
  void RL_TIMER(int rlIdx);		// RL timer algorithm
  void ScheduleTimer(int rlIdx, double expiredPeriod, bool resetEvent);

  // QCN-TRAFFIC-SHAPER -------------------------------------------------------
  uint64_t tsBacklogMaxQsize;
  uint64_t tsBacklogQsize;
  std::queue<Ptr<Packet> > tsBacklogQ;

  #define TS_TIME_INIT 1/100000000
  bool shapperRunning;
  bool tsBacklogEnqueue (Ptr<Packet> p);
  void TrafficShaper(void);
  double TrafficShaperSpeed(Ptr<Packet> p, bool dropPkt);

  // Logging ------------------------------------------------------------------
  std::string vbrFileString;
  std::string logFileString;
  std::string vbrSimTimeString;

  FILE *vbrF;
  FILE *vbrSimTimeF;
  FILE *logF;
};

} // namespace ns3

#endif /* QCNRP_Q_H */
