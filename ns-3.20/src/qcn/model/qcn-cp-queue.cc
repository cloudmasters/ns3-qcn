#include <math.h>

#include "ns3/log.h"
#include "ns3/enum.h"
#include "ns3/uinteger.h"

#include "ns3/qcn-fb-proto.h"
#include "ns3/ethernet-header.h"
#include "ns3/net-device.h"
#include "ns3/csma-net-device.h"
#include "ns3/channel.h"
#include "ns3/qcn-cp-queue.h"

#include <stdio.h>
#include <stdlib.h>

// Enable proper debug to print required information
// -------------------------------------------------
//#define CP_QCN_LOG
//#define CP_LOG_AGING_LOG
//#define CP_LOG_FLOWSHARE_LOG

NS_LOG_COMPONENT_DEFINE ("QCNCPQueue");

namespace ns3 {

NS_OBJECT_ENSURE_REGISTERED (QCNCPQueue);

TypeId QCNCPQueue::GetTypeId (void) 
{
  static TypeId tid = TypeId ("ns3::QCNCPQueue")
    .SetParent<Queue> ()
    .AddConstructor<QCNCPQueue> ()
    .AddAttribute ("MaxBytes", 
                   "The maximum number of bytes accepted by this QCNCPQueue.",
                   UintegerValue (100 * 65535),
                   MakeUintegerAccessor (&QCNCPQueue::m_maxBytes),
                   MakeUintegerChecker<uint32_t> ())
  ;
  return tid;
}

QCNCPQueue::QCNCPQueue () :
  Queue (),
  m_packets (),
  m_bytesInQueue (0)
{
  NS_LOG_FUNCTION (this);
  m_mode = QUEUE_MODE_BYTES;

  // QCN parameters initialization
  isCPActive = false;

  m_Qeq = 33000; // Asuming: Q-MAX-LEN = 1500*100bytes
  m_W = 2;
  m_Q = 0;
  m_Qold = 0;
#define CP_SAMPLING_RATE 1500 /* MTU */
  timeToMark = CP_SAMPLING_RATE;

//  agingTimeWindow = 1000000;/* 1s    = 1000000us */
//  agingTimeWindow = 500000; /* 0.5s  = 500000us */
//  agingTimeWindow = 250000; /* 0.25s = 250000us */
//  agingTimeWindow = 100000; /* 0.10s = 100000us */
  agingTimeWindow = 10000; /* 10ms = 10000us */
  agingTimer = Timer(Timer::CANCEL_ON_DESTROY);
  agingTimerRunning = false;
}

QCNCPQueue::~QCNCPQueue ()
{
  NS_LOG_FUNCTION (this);
}

void
QCNCPQueue::SetNetDev(Ptr<NetDevice> QnetDev)
{
  NS_LOG_FUNCTION (this);
  m_qnetdev = QnetDev;

  // Logging
  // ---------------------------------------------------------------------
  // write throughput data to files - each file name is the MAC address:
  //   e.g. 00000e or 000AABBFF... etc
  Ptr<CsmaNetDevice> csmaNetDevice = m_qnetdev->GetObject<CsmaNetDevice> ();
  Address csmaNetDevAddr = csmaNetDevice->GetAddress();
  uint8_t csmaNetDevAddrBuf[64];
  uint32_t addrLen = csmaNetDevAddr.CopyTo(csmaNetDevAddrBuf);
  
  std::stringstream convertToString;
  for (uint32_t y=0; y<addrLen; y++)
   convertToString<<((uint32_t)csmaNetDevAddrBuf[y]);

  queueFileString = "./qcn/cp_";
  queueFileString.append(convertToString.str());
  queueFileString.append("_queue_variation");

  logFileString = "./qcn/cp_";
  logFileString.append(convertToString.str());
  logFileString.append("_log");
}

void
QCNCPQueue::SetQeq (int Qeq_set)
{
  NS_LOG_FUNCTION (this);
  m_Qeq = Qeq_set;
}

int
QCNCPQueue::GetQeq (void)
{
  NS_LOG_FUNCTION (this);
  return m_Qeq;
}

void
QCNCPQueue::SetW (int W)
{
  NS_LOG_FUNCTION (this);
  m_W = W;
}

int
QCNCPQueue::GetW (void)
{
  NS_LOG_FUNCTION (this);
  return m_W;
}

bool 
QCNCPQueue::DoEnqueue (Ptr<Packet> p)
{
  NS_LOG_FUNCTION (this << p);

  if (isCPActive) {
    // QCN-FRE: Check if this is a new flow (new SRC MAC) and add entry if YES
    QCN_FRE_UpdateFlowShareHTable(p);

    // QCN-CP
    if (QCN_ComputeFeedback(p))
      QCN_SendFeedback(p);

    // QCN-FRE aging tables
    if (!agingTimerRunning) {
      agingTimerRunning = true;
      ScheduleAgingTimer();
    }
  }

  if (m_mode == QUEUE_MODE_PACKETS && (m_packets.size () >= m_maxPackets))
    {
      std::cout << "QUEUE" << logFileString.c_str()
                << ":CP: Queue full (at max packets:"
                << m_maxPackets << ") -- droppping pkt" << std::endl;
      NS_LOG_LOGIC ("Queue full (at max packets) -- droppping pkt");
      Drop (p);
      return false;
    }

  if (m_mode == QUEUE_MODE_BYTES && (m_bytesInQueue + p->GetSize () >= m_maxBytes))
    {
      std::cout << "QUEUE" << logFileString.c_str();
      std::cout << ":CP: Queue full (packet would exceed max bytes"
                << m_maxBytes << ") -- droppping pkt" << std::endl;
      NS_LOG_LOGIC ("Queue full (packet would exceed max bytes) -- droppping pkt");
      Drop (p);
      return false;
    }

  if (isCPActive) {
    // log Q variation into file for graphs
    cpLogQVariation();
  }

  m_bytesInQueue += p->GetSize ();
  m_packets.push (p);

  NS_LOG_LOGIC ("Number packets " << m_packets.size ());
  NS_LOG_LOGIC ("Number bytes " << m_bytesInQueue);

  return true;
}

Ptr<Packet>
QCNCPQueue::DoDequeue (void)
{
  NS_LOG_FUNCTION (this);

  if (m_packets.empty ())
    {
      NS_LOG_LOGIC ("Queue empty");
      return 0;
    }

  Ptr<Packet> p = m_packets.front ();
  m_packets.pop ();
  m_bytesInQueue -= p->GetSize ();

  NS_LOG_LOGIC ("Popped " << p);

  NS_LOG_LOGIC ("Number packets " << m_packets.size ());
  NS_LOG_LOGIC ("Number bytes " << m_bytesInQueue);

  return p;
}

Ptr<const Packet>
QCNCPQueue::DoPeek (void) const
{
  NS_LOG_FUNCTION (this);

  if (m_packets.empty ())
    {
      NS_LOG_LOGIC ("Queue empty");
      return 0;
    }

  Ptr<Packet> p = m_packets.front ();

  NS_LOG_LOGIC ("Number packets " << m_packets.size ());
  NS_LOG_LOGIC ("Number bytes " << m_bytesInQueue);

  return p;
}


// QCN - CP -------------------------------------------------------------------
//-----------------------------------------------------------------------------
void 
QCNCPQueue::ActiveCP(bool isCP)
{
  isCPActive = isCP;
  return;
}

bool
QCNCPQueue::QCN_ComputeFeedback(Ptr<Packet> p)
{
  NS_LOG_FUNCTION (this);

  bool sendFb = false;
  int Fb_max = 0;

  // Compute Fb: !Mind that Fb si computed as positive, rather than -Fb
  m_Q      = m_bytesInQueue + p->GetSize ();
  m_Qdelta = m_Q - m_Qold;
  m_Qoff   = m_Q - m_Qeq;
  Fb       = m_Qoff + m_W*m_Qdelta;
  Fb_max   = m_Qeq*(2*m_W + 1);

  // The maximum value of Fb determines the number of bits that Fb uses.
  // Uniform quantization of Fb, qntz_Fb, uses most significant bits of Fb.
  qntz_Fb = 0;
  if (Fb > Fb_max)
    Fb = Fb_max;
  else if (Fb > 0)
    qntz_Fb = 63*Fb/Fb_max;

#if defined(CP_QCN_LOG)
	logFile = fopen(logFileString.c_str(), "a");
		fprintf(logFile, "--> CP Simu at:%lldus\n", Simulator::Now().GetMicroSeconds());
		fprintf(logFile,"  > Q:%d\n", m_Q);
		fprintf(logFile,"  > Qold:%d\n", m_Qold);
		fprintf(logFile,"  > Fb:%d\n", Fb);
		fprintf(logFile,"  > qntzFb:%d\n", qntz_Fb);
		fprintf(logFile, " >\n");
	fclose(logFile);
#endif

  // Sampling rate: 1.5KB
  timeToMark -= p->GetSize ();
  if (timeToMark <= 0 && qntz_Fb > 0) {
    timeToMark = CP_SAMPLING_RATE;
    m_Qold = m_Q;

    if (qntz_Fb > 0)
      sendFb = true;
  }

  // QCN-FRE: update tables for Flow-share calculus
  QCM_FRE_UpdateFlowTables(p);

  if (sendFb) {
#if defined(CP_QCN_LOG)
	EthernetHeader pEthH;
	p->PeekHeader(pEthH);
	Mac48Address pMacSRC = pEthH.GetSource();
	uint8_t srcMacBuf[6];
	pMacSRC.CopyTo(srcMacBuf);

	logFile = fopen(logFileString.c_str(), "a");
		fprintf(logFile, "--> Feedback to:");
		for ( int t=0; t<6; t++)
			fprintf(logFile, "%u", srcMacBuf[t]);
		fprintf(logFile, " at:%lldus\n", Simulator::Now().GetMicroSeconds());
		fprintf(logFile,"  > Q         :%d\n", m_Q);
		fprintf(logFile,"  > Fb        :%d\n", Fb);
		fprintf(logFile,"  > qntz_Fb   :%d\n", qntz_Fb);
		fprintf(logFile,"--> Feedback-SENT\n");
	fclose(logFile);
#endif
    return true;
  }

  return false;
}

void
QCNCPQueue::QCN_SendFeedback(Ptr<Packet> p)
{
  EthernetHeader pEthH;
  p->PeekHeader(pEthH);
  Mac48Address pMacSRC = pEthH.GetSource();

  // prepare QCN Fb packet
  Ptr<Packet> Fbp = Create<Packet> ();
  QCNFbHeader FbQCNH;
  uint8_t srcMacBuf[6];
  pMacSRC.CopyTo(srcMacBuf);
  FbQCNH.SetFlowID(0);
  FbQCNH.SetQnzFb(qntz_Fb);
  Fbp->AddHeader(FbQCNH);

  //Get the BridgeNetDevice to which this queue belongs to - do it only once
  //(even though in real life bridges can be dynamic in our case they are only initilised once)
  if(!m_brnetdev) {
    Ptr<Node> swNode = m_qnetdev->GetNode();//the switch itself

    //Iterate through the NetDevicies of the node, but only some are bridges
    //and only one bridge has this CP port associated
    for(uint32_t i=0; i<swNode->GetNDevices(); i++) {
      Ptr<NetDevice> swNetDevice = swNode->GetDevice(i);
      if(swNetDevice->IsBridge()){ //our candidate is a bridge!
      
        { Ptr<BridgeNetDevice> swBridge = swNetDevice->GetObject<BridgeNetDevice>();
	if (swBridge)
          for(uint32_t j=0; j<swBridge->GetNBridgePorts(); j++) {
            if(swBridge->GetBridgePort(j) == m_qnetdev) { //found our bridge!
              m_brnetdev = swBridge;
              std::cout << "Send feedback to Bridge" << std::endl;
              break;
            }
          }
	}

        { Ptr<OpenFlowSwitchNetDevice> swSwitch = swNetDevice->GetObject<OpenFlowSwitchNetDevice>();
	if (swSwitch)
          for(uint32_t j=0; j<swSwitch->GetNSwitchPorts(); j++) {
            if(swSwitch->GetSwitchPort(j).netdev == m_qnetdev) { //found our bridge!
              m_brnetdev = swSwitch;
              std::cout << "Send feedback to OpenFlowSwitch" << std::endl;
              break;
            }
          }
        }

        if (m_brnetdev) break;
      }
    }

    if(!m_brnetdev) {
      std::cout << "BridgeNetDevice/OpenFlowSwitchNetDevice not found... "
                << "this device is not part of bridge..."
                << "where should I send the feedback? Add the dev to a BridgeNetDevice/OpenFlowNetDevice" << std::endl;
      exit(1);
    }
  }

#if defined(CP_QCN_LOG)
  printMAC("-Feedback send to: ", pMacSRC, "");
#endif

  //Send the packet to the BridgeNetDevice for processing and forwarding on the correct port
  //Note: this simulates sending a message from the CPU to the FDB
  m_brnetdev->Send (Fbp,      //the packet with Fb
                    pMacSRC,  //the initial packet MACSRC is the Fb MACDST
                    0xFAC);   //invented by me for simulation
  return;
}

// QCN - FRE ------------------------------------------------------------------
void
QCNCPQueue::QCN_FRE_UpdateFlowShareHTable(Ptr<Packet> p)
{
  // if this is a new flow, then create a new entry
  if (false == QCN_FRE_FindFlow(p))
    QCN_FRE_AddFlow(p);
}

bool
QCNCPQueue::QCN_FRE_FindFlow(Ptr<Packet> p)
{
  // each flow is identified by MAC SRC
  EthernetHeader pEthH;
  p->PeekHeader(pEthH);
  Mac48Address pMacSRC = pEthH.GetSource();

  if (flowShareHTable.empty())
    return false;

  FlowShareHTable_it_t it = flowShareHTable.find(pMacSRC);
  if (it != flowShareHTable.end()) {
#if defined(CP_LOG_AGING_LOG)
    printMAC("-Flow found: ",pMacSRC, "");
#endif
    return true;
  }

#if defined(CP_LOG_AGING_LOG)
  printMAC("-Flow NOT found: ",pMacSRC, "");
#endif
  return false;
}

void
QCNCPQueue::QCN_FRE_AddFlow(Ptr<Packet> p)
{
  // each flow is identified by MAC SRC
  EthernetHeader pEthH;
  p->PeekHeader(pEthH);
  Mac48Address pMacSRC = pEthH.GetSource();

  // add new flow to Feedbak Table and Flow Share Table
  fbHTable.insert(std::pair<Mac48Address, FbHStatTable_t *>
                           (pMacSRC     , new FbHStatTable_t));
  flowShareHTable.insert(std::pair<Mac48Address,int64_t>
                                  (pMacSRC     , 0));
  // Update Q rank
  q_Rank = flowShareHTable.size();

#if defined(CP_LOG_AGING_LOG)
  printMAC("-Flow added: ", pMacSRC, "");
#endif
}

void
QCNCPQueue::QCM_FRE_UpdateFlowTables(Ptr<Packet> p)
{
  if (fbHTable.empty())
    return;

  // each flow is identified by MAC SRC
  EthernetHeader pEthH;
  p->PeekHeader(pEthH);
  Mac48Address pMacSRC = pEthH.GetSource();

  // simulation time when Feedback is calculated
  int64_t c_time= Simulator::Now().GetMicroSeconds();

  // add new statistic for current flow (pMacSRC)
  FbHStatTable_t *fbHStatTable = fbHTable[pMacSRC];
  fbHStatTable->insert(std::pair<int64_t, int> (c_time, Fb));

  // compute Flow-share
  int64_t timedFb = 0;
  int64_t timed = 0;
  FbHStatTable_it_t first_it = fbHStatTable->begin();
  for (FbHStatTable_it_t it = fbHStatTable->begin(); it != fbHStatTable->end(); ++it){
    // normalize time
    int64_t nTime = it->first - first_it->first;

    // ! Mind that Fb is stored in positive form, therfore:
    //   Fb > 0 ==>    congestion
    //   Fb < 0 ==> NO congestion
    timedFb += ((int64_t)(it->second)*nTime);
    timed += nTime;
  }

  // ! Mind that:
  //   BIGGER  Fb ==> BIGGER congestion
  //   SMALLER Fb ==> SMALLER congestion
  if (timed)
    flowShareHTable[pMacSRC] = (int64_t)((timedFb*q_Rank)/timed);
  else
    flowShareHTable[pMacSRC] = 0;

#if defined(CP_LOG_FLOWSHARE_LOG)
  printTables();
#endif
}

bool
QCNCPQueue::QCN_FRE_AgingFbStatTable(Mac48Address pMacSRC, FbHStatTable_t *fbHStatTable, int64_t c_time)
{
  int64_t s_time = c_time - agingTimeWindow;

  for (FbHStatTable_it_t it = fbHStatTable->begin(); it != fbHStatTable->end(); ++it){
    if (it->first < s_time) {
#if defined(CP_LOG_AGING_LOG)
      printMAC("-aged entry:", pMacSRC, "");
      printLogInt("", it->first, "");
#endif

      fbHStatTable->erase(it);
    }
  }

  // after aging the table may be empty
  if (fbHStatTable->empty())
    return true;

  return false;
}

void
QCNCPQueue::QCN_FRE_AgingFlowTables(int64_t c_time)
{
#if defined(CP_LOG_AGING_LOG)
  printLog("-Aging...");
#endif

  // aging tables - clean old entries
  FbHTable_it_t fb_it; 
  for (fb_it = fbHTable.begin(); fb_it != fbHTable.end(); ++fb_it ){
    FbHStatTable_t *fbHStatTable = fb_it->second;

    // if table is empty, then delete the entry from both
    //   Feedback Statistics table and Flow Share table
    if (QCN_FRE_AgingFbStatTable(fb_it->first, fbHStatTable, c_time)){
#if defined(CP_LOG_AGING_LOG)
      printMAC("-Feedback Statistics Table: EMPTY for:", fb_it->first, "");
#endif

      flowShareHTable.erase(fb_it->first);
      fbHTable.erase(fb_it->first);
    }
  }

  // schedule next aging session
  ScheduleAgingTimer();
}

void
QCNCPQueue::ScheduleAgingTimer(void)
{
  NS_LOG_FUNCTION (this);

  // Do not resechedule while running
  if (agingTimer.IsRunning())
    return;

  // Schedule agingTimer
  agingTimer.SetFunction (&QCNCPQueue::QCN_FRE_AgingFlowTables, this);
  agingTimer.SetArguments (Simulator::Now().GetMicroSeconds());
  agingTimer.Schedule (MicroSeconds(agingTimeWindow));
}

// Debuging & Loging ----------------------------------------------------------
void
QCNCPQueue::printLog(const char *log)
{
  std::cout << log << std::endl;
}

void
QCNCPQueue::printLogInt(const char *preLog, int x, const char *poslog)
{
  std::cout << preLog << x << poslog << std::endl;
}

void
QCNCPQueue::printLogInt64(const char *preLog, int64_t x, const char *poslog)
{
  std::cout << preLog << x << poslog << std::endl;
}

void
QCNCPQueue::printMAC(const char *preLog, Mac48Address pMacSRC, const char *poslog)
{
  uint8_t srcMacBuf[6];

  std::cout << preLog;
  pMacSRC.CopyTo(srcMacBuf);
  for ( int t=0; t<6; t++)
    std::cout << (uint32_t)srcMacBuf[t];
  std::cout << poslog << std::endl;
}

void
QCNCPQueue::printTables(void)
{
  FbHTable_it_t fb_it;
  FlowShareHTable_it_t sh_it;

  printLog("--TABLES DUMP--");
  for (fb_it = fbHTable.begin(), sh_it = flowShareHTable.begin();
       fb_it != fbHTable.end(), sh_it != flowShareHTable.end();
       ++fb_it, ++sh_it){
    FbHStatTable_t *fbHStatTable = fb_it->second;
    printMAC("Feedback Statistics Table for:", fb_it->first, " dump:");

    FbHStatTable_it_t it;
    FbHStatTable_it_t first_it = fbHStatTable->begin();
    for (it = fbHStatTable->begin(); it != fbHStatTable->end(); ++it){
      printLogInt64("   -entry time: ", it->first - first_it->first, ":");
      printLogInt  ("   -entry Fb  : ", it->second, ":");
    }
    printLogInt64("   --share:", sh_it->second, "");
  }
}

void 
QCNCPQueue::cpLogQVariation(void)
{
  queueFile = fopen(queueFileString.c_str(), "a");
  fprintf(queueFile, "%ld %d\n", Simulator::Now().GetMicroSeconds() ,m_bytesInQueue);
  fclose(queueFile);
}

} // namespace ns3

