#ifndef QCNCP_Q_H
#define QCNCP_Q_H

#include <queue>
#include "ns3/packet.h"
#include "ns3/queue.h"

#include <map>
#include "ns3/net-device.h"
#include "ns3/bridge-net-device.h"
#include "ns3/openflow-switch-net-device.h"
#include "ns3/timer.h"
#include "ns3/mac48-address.h"

namespace ns3 {

#define MAX_FLOWS 128
#define MAX_FBS_PER_FLOW 512

#define FLOW_ACTIVE true
#define FLOW_INACTIVE false

// Hash tables definitions for QCN-FRE algorithm
/*
 +-------------------------------------------------+
 |                  FlowShare_t                    |
 +--------------------------------+----------------+
 |         FbHTable_t             |                | 
 +--------------+-----------------+                |
 |              |                 |   Flow-share   |
 | Flow IDs:MAC |  FbHStatTable_t |                |
 |              |                 |                |
 +--------------+-----------------+----------------+
 |              |t0   t1  ... tn  |                |
 | FlowID0 (MAC)|                 | Flow-share-ID0 |
 |              |Fb0  Fb1 ... Fbn |                |
 +--------------+-----------------+----------------+
 |              |t0   t1  ... tn  |                |
 | FlowID1 (MAC)|                 | Flow-share-ID1 |
 |              |Fb0  Fb1 ... Fbn |                |
 +--------------+-----------------+----------------+
 |       ...                                       |
*/
typedef std::map<int64_t, int> 
        FbHStatTable_t;	  // Feedback Statistics Table: pair (Time.us, Fb)
typedef std::map<Mac48Address, FbHStatTable_t *>
        FbHTable_t;	  // Feedbak Table: pair (FlowID-MAC, Feedback Statistics table)
typedef std::map<Mac48Address, int64_t>
        FlowShareHTable_t;// Flow Share Table: pair (FlowID:MAC, Flow-share)

typedef std::map<int64_t, int>::iterator
        FbHStatTable_it_t;   // Feedback Statistics table iterator
typedef std::map<Mac48Address, FbHStatTable_t *>::iterator
        FbHTable_it_t;       // Feedbak Table iterator
typedef std::map<Mac48Address, int64_t>::iterator
        FlowShareHTable_it_t;// Flow Share Table iterator

class TraceContainer;

class QCNCPQueue : public Queue {
public:
  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);

  QCNCPQueue ();
  virtual ~QCNCPQueue();

  void SetNetDev(Ptr<NetDevice> QnetDev);
  void ActiveCP(bool isCP);// call to make Q congestion aware
  void SetQeq(int Qeq);    // unused
  int GetQeq();
  void SetW(int W);        // unused
  int GetW();

private:
  virtual bool DoEnqueue (Ptr<Packet> p);
  virtual Ptr<Packet> DoDequeue (void);
  virtual Ptr<const Packet> DoPeek (void) const;

  std::queue<Ptr<Packet> > m_packets; //!< the packets in the queue
  uint32_t m_maxPackets;              //!< max packets in the queue
  uint32_t m_maxBytes;                //!< max bytes in the queue
  uint32_t m_bytesInQueue;            //!< actual bytes in the queue
  QueueMode m_mode;                   //!< queue mode (packets or bytes limited)

  // QCN - CP ------------------------------------------------------------
  bool isCPActive;// activate Q as congestion controlled, otherwise is a tail drop Q.
  int m_Qeq;      // the reference point of a queue. QCN aims to keep 
                  //   the queue occupancy at this reference level under congestion.
  int m_W;        // the control parameter in calculating the congestion level variable Fb.
  int m_Q;        // instant current queue utilization.
  int m_Qold;     // anterior queue utilization.
  int m_Qdelta;   // rate excess.
  int m_Qoff;     // size excess.
  int Fb;         // feedback.
  int qntz_Fb;    // quantized feedback.
  int timeToMark; // threshold to send feedback to RP source.
  bool QCN_ComputeFeedback(Ptr<Packet> p);// Compute Fb
  void QCN_SendFeedback(Ptr<Packet> p);   // Sent Fb feedback frame to source

  // QCN - FRE ----------------------------------------------------------------
  int32_t q_Rank;

  FbHTable_t        fbHTable;				// Feedbak Table
  FlowShareHTable_t flowShareHTable;			// Flow Share Table
  void QCN_FRE_UpdateFlowShareHTable(Ptr<Packet> p);	// Update flow table
  bool QCN_FRE_FindFlow(Ptr<Packet> p);			// Seek a flow
  void QCN_FRE_AddFlow(Ptr<Packet> p);			// Add new flow
  void QCM_FRE_UpdateFlowTables(Ptr<Packet> p);		// Update QCN-FRE tables

  // QCN - FRE - Aging tables...
  int32_t agingTimeWindow;
  Timer agingTimer;
  bool agingTimerRunning;
  void ScheduleAgingTimer(void);
  bool QCN_FRE_AgingFbStatTable(Mac48Address pMacSRC, FbHStatTable_t *fbHStatTable, int64_t c_time);
  void QCN_FRE_AgingFlowTables(int64_t c_time);		// Remove entries older
							//   than c_time-agingTimeWindow

  Ptr<NetDevice> m_qnetdev;
  Ptr<NetDevice> m_brnetdev;
//  Ptr<BridgeNetDevice> m_brnetdev;
//  Ptr<OpenFlowSwitchNet Device> m_ofswnetdev;

  // Debuging & Loging --------------------------------------------------------
  std::string queueFileString;
  std::string logFileString;

  FILE *queueFile;
  FILE *logFile;
  void cpLogQVariation(void);

  void printTables(void);
  void printMAC(const char *preLog, Mac48Address pMacSRC, const char *poslog);
  void printLogInt(const char *preLog, int x, const char *poslog);
  void printLogInt64(const char *preLog, int64_t x, const char *poslog);
  void printLog(const char *log);
};

} // namespace ns3

#endif /* QCNCP_Q_H */
