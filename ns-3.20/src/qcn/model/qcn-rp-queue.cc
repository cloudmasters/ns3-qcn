#include "ns3/log.h"
#include "ns3/enum.h"
#include "ns3/uinteger.h"
#include "ns3/qcn-rp-queue.h"
#include "ns3/qcn-fb-proto.h"

#include "ns3/node.h"
#include "ns3/backoff.h"
#include "ns3/address.h"
#include "ns3/net-device.h"
#include "ns3/callback.h"
#include "ns3/packet.h"
#include "ns3/traced-callback.h"
#include "ns3/nstime.h"
#include "ns3/data-rate.h"
#include "ns3/ptr.h"
#include "ns3/mac48-address.h"
#include "ns3/ethernet-header.h"

#include "ns3/channel.h"
#include "ns3/csma-channel.h"
#include "ns3/csma-net-device.h"

#include "ns3/simulator.h"
#include "ns3/timer.h"

#include "ns3/core-module.h"

#include <stdio.h>
#include <stdlib.h>

NS_LOG_COMPONENT_DEFINE ("QCNRPQueue");

namespace ns3 {

//#define RP_LOG

NS_OBJECT_ENSURE_REGISTERED (QCNRPQueue);

TypeId QCNRPQueue::GetTypeId (void) 
{
  static TypeId tid = TypeId ("ns3::QCNRPQueue")
    .SetParent<Queue> ()
    .AddConstructor<QCNRPQueue> ()
    .AddAttribute ("MaxBytes", 
                   "The maximum number of bytes accepted by this QCNRPQueue.",
                   UintegerValue (100 * 65535),
                   MakeUintegerAccessor (&QCNRPQueue::m_maxBytes),
                   MakeUintegerChecker<uint32_t> ())
  ;
  return tid;
}

QCNRPQueue::QCNRPQueue () :
  Queue (),
  m_packets (),
  m_bytesInQueue (0)
{
  NS_LOG_FUNCTION (this);
  m_mode = QUEUE_MODE_BYTES;

  isRPActive = false;

  // QCN - RP - RLs initialization
  for (int i=0; i<MAX_RL; i++) {
    RL[i].state = INACTIVE;
    RL[i].qlen = 0;
    RL[i].flowID = 0;

    RL[i].CR = C;
    RL[i].TR = C;

    RL[i].txByteCnt = BYTE_CNT_TH;
    RL[i].RLByteCntCycle = 0;

    RL[i].RLTimer = Timer(Timer::CANCEL_ON_DESTROY);
    RL[i].RLTimerCycle = 1;
  }

  oldrlSTATE = -1;
  rlSTATE = -1;

  shapperRunning = false;
  tsBacklogMaxQsize = 1500 * 1000000;
}

QCNRPQueue::~QCNRPQueue ()
{
  NS_LOG_FUNCTION (this);
}

void
QCNRPQueue::SetNetDev(Ptr<NetDevice> QnetDev)
{
  NS_LOG_FUNCTION (this);

  // get the line rate
  m_qnetdev = QnetDev;
  
  // Logging
  // ---------------------------------------------------------------------
  // write throughput data to files - each file name is the MAC address:
  //   e.g. 00000e or 000AABBFF... etc
  Ptr<CsmaNetDevice> csmaNetDevice = m_qnetdev->GetObject<CsmaNetDevice> ();
  Address csmaNetDevAddr = csmaNetDevice->GetAddress();
  uint8_t csmaNetDevAddrBuf[64];
  uint32_t addrLen = csmaNetDevAddr.CopyTo(csmaNetDevAddrBuf);
  
  std::stringstream convertToString;
  for (uint32_t y=0; y<addrLen; y++)
   convertToString<<((uint32_t)csmaNetDevAddrBuf[y]);

  vbrFileString = "./qcn/rp_";
  vbrFileString.append(convertToString.str());
  vbrFileString.append("-vbr");

  logFileString = "./qcn/rp_";
  logFileString.append(convertToString.str());
  vbrSimTimeString.append("-vbr-time");

  vbrSimTimeString = "./qcn/rp_";
  vbrSimTimeString.append(convertToString.str());
  logFileString.append("-log");
}

Ptr<NetDevice>
QCNRPQueue::GetNetDev(void)
{
  return m_qnetdev;
}

bool 
QCNRPQueue::DoEnqueue (Ptr<Packet> p)
{
  NS_LOG_FUNCTION (this << p);

  // if REACTION POINT activated, then enqueue frame to traffic shapper
  if (isRPActive)
    return tsBacklogEnqueue(p);

  if (m_mode == QUEUE_MODE_PACKETS && (m_packets.size () >= m_maxPackets))
    {
      std::cout << "RP: Queue full (at max packets:"
                << m_maxPackets << ") -- droppping pkt" << std::endl;
      Drop (p);
      return false;
    }

  if (m_mode == QUEUE_MODE_BYTES && (m_bytesInQueue + p->GetSize () >= m_maxBytes))
    {
      std::cout << "RP: Queue full (packet would exceed max bytes:"
                << m_bytesInQueue + p->GetSize () << ") -- droppping pkt" << std::endl;
      Drop (p);
      return false;
    }

  m_bytesInQueue += p->GetSize ();
  m_packets.push (p);

  NS_LOG_LOGIC ("Number packets " << m_packets.size ());
  NS_LOG_LOGIC ("Number bytes " << m_bytesInQueue);

  NS_LOG_LOGIC("Called: DoEnqueue\n");
  return true;
}

Ptr<Packet>
QCNRPQueue::DoDequeue (void)
{
  NS_LOG_FUNCTION (this);

  if (m_packets.empty ())
    {
      NS_LOG_LOGIC ("Queue empty");
      return 0;
    }

  Ptr<Packet> p = m_packets.front ();
  m_packets.pop ();
  m_bytesInQueue -= p->GetSize ();

  NS_LOG_LOGIC ("Popped " << p);

  NS_LOG_LOGIC ("Number packets " << m_packets.size ());
  NS_LOG_LOGIC ("Number bytes " << m_bytesInQueue);

  NS_LOG_LOGIC("Called: DoDequeue\n");
  return p;
}

Ptr<const Packet>
QCNRPQueue::DoPeek (void) const
{
  NS_LOG_FUNCTION (this);

  if (m_packets.empty ())
    {
      NS_LOG_LOGIC ("Queue empty");
      return 0;
    }

  Ptr<Packet> p = m_packets.front ();

  NS_LOG_LOGIC ("Number packets " << m_packets.size ());
  NS_LOG_LOGIC ("Number bytes " << m_bytesInQueue);

  return p;
}

// QCN-REACTION-POINT----------------------------------------------------------
int
QCNRPQueue::GetFlowID (Ptr<Packet> p)
{
  NS_LOG_FUNCTION (this);

  return 0;
}

int
QCNRPQueue::GetRLIdx (int flowID)
{
  NS_LOG_FUNCTION (this);

  return flowID;
}

void
QCNRPQueue::ActiveRP(bool statusRP)
{
  // this is shit... upclass Queue needs to know due to traffic shapper
  isRPActive = statusRP;
  isRP = statusRP;
}

void
QCNRPQueue::ActivateRL(int rlIdx, int flowID)
{
  RL[rlIdx].state  = ACTIVE;
  RL[rlIdx].flowID = flowID;
  RL[rlIdx].CR = C;
  RL[rlIdx].TR = C;
  RL[rlIdx].RLByteCntCycle = 1;
  RL[rlIdx].RLTimerCycle   = 1;
  RL[rlIdx].txByteCnt = BYTE_CNT_TH;
}

void
QCNRPQueue::DeactivateRL (int rlIdx) 
{
  RL[rlIdx].state = INACTIVE;
  RL[rlIdx].flowID = -1;
  RL[rlIdx].CR = C;
  RL[rlIdx].TR = C;
  RL[rlIdx].txByteCnt = BYTE_CNT_TH;
  RL[rlIdx].RLByteCntCycle = 1;
  RL[rlIdx].RLTimerCycle = 1;
}

void
QCNRPQueue::Rcv_Fbfrm ( Ptr<NetDevice> device, Ptr<const Packet> packet,
                        uint16_t protocol, const Address &from,
                        const Address &to, NetDevice::PacketType packetType)
{
  NS_LOG_FUNCTION (this);

  QCNFbHeader FbQCNH;
  packet->PeekHeader(FbQCNH);
  Ptr<Packet> p = packet->Copy();

  r_qnzFb    = FbQCNH.GetQnzFb();
  r_flowID   = FbQCNH.GetFlowID();
  int flowID = GetFlowID(p);
  int rlIdx  = GetRLIdx(flowID);

  // Fb received, so activate RL if need to
  if (RL[rlIdx].state == INACTIVE)
    ActivateRL(rlIdx, flowID);

  // DECREASE PHASE: Fb received
  //----------------------------

  // Set TR and decrease CR
  double decreaseFactor = (1-GD*r_qnzFb);
  RL[rlIdx].TR = RL[rlIdx].CR;
  RL[rlIdx].CR = RL[rlIdx].CR * decreaseFactor;

  // Current rate cannot decrease less than MIN_RATE
  if ( RL[rlIdx].CR < MIN_RATE )
    RL[rlIdx].CR = MIN_RATE;

  // Initialize byte-counter threshold
  RL[rlIdx].txByteCnt = BYTE_CNT_TH;

  // Reset byte counter and timer cycles.
  RL[rlIdx].RLByteCntCycle = 1;
  RL[rlIdx].RLTimerCycle   = 1;

  // Reset RL timer
  ScheduleTimer(rlIdx, TIMER_TH, true);
}

void 
QCNRPQueue::RL_BYTE_COUNTER(Ptr<Packet> p)
{
  NS_LOG_FUNCTION (this);

  int flowID = GetFlowID(p);
  int rlIdx  = GetRLIdx(flowID);

  if (RL[rlIdx].state == INACTIVE)
    return;

  // If CR is line rate or queue is empty, then deactivate RL
  if (RL[rlIdx].CR == C && RL[rlIdx].qlen == 0) {
    DeactivateRL(rlIdx);
    return;
  }

  // Update byte-counter
  RL[rlIdx].txByteCnt -= p->GetSize ();

  // byte couter threshold reached -> next increase phase
  if (RL[rlIdx].txByteCnt < 0) {
    int byteCntThreshold = 0;

    // If FAST RECOVERY PHASE, then BC THERSHOLD is BYTE_CNT_TH (1500*100)
    //   if ACTIVE or HYPER-ACTIVE INCREASE then  BYTE_CNT_TH/2 (1500*50)
    if (RL[rlIdx].RLByteCntCycle < FR_CYCLE_TH)
      byteCntThreshold = (int)BYTE_CNT_TH;
    else
      byteCntThreshold = (int)(BYTE_CNT_TH/2);

    // Update BC (count cycle and reset byte counter)
    RL[rlIdx].RLByteCntCycle++;
    RL[rlIdx].txByteCnt = byteCntThreshold;

    // Next PHASE
    RL_UPDATE_PHASE(rlIdx);
  }
}

void 
QCNRPQueue::RL_UPDATE_PHASE (int rlIdx) 
{
  NS_LOG_FUNCTION (this);
  int Ri = 0; // Increase factor - R_AI or R_HAI


  // Set RL RATE-INCREASE PHASEs:
  // ----------------------------

  // FAST RECOVERY: if both BC and TIMER are in FR PHASE
  rlSTATE = RECOVERY_P;
  Ri = 0;

  if ( RL[rlIdx].RLByteCntCycle > FR_CYCLE_TH ||
       RL[rlIdx].RLTimerCycle   > FR_CYCLE_TH   ) {

    // ACTIVE INCREASE: if BC or TIMER is in AI PHASE
    rlSTATE = ACTIVE_P;
    Ri = R_AI;

    if ( RL[rlIdx].RLByteCntCycle > FR_CYCLE_TH &&
         RL[rlIdx].RLTimerCycle   > FR_CYCLE_TH    ) {

      // HYPER-ACTIVE INCREASE: if both BC and TIMER are in HAI PHASE
      rlSTATE = HYPER_ACTIVE_P;
      Ri = R_HAI;
    }
  }

  // Update TR and CR:
  RL[rlIdx].TR = RL[rlIdx].TR + Ri;
  RL[rlIdx].CR = (RL[rlIdx].TR + RL[rlIdx].CR)/2;

  // Make sure the CR is not bigger than the line rate (C)
  if (RL[rlIdx].CR > C)
    RL[rlIdx].CR = C;
}

void 
QCNRPQueue::RL_TIMER(int rlIdx)
{
  NS_LOG_FUNCTION (this);

  double expiredPeriod = 0;

  // If RL is not active, return
  if (RL[rlIdx].state != ACTIVE)
    return;

  // RL-TIMER expired, so count the cycle and Next PHASE
  RL[rlIdx].RLTimerCycle++;
  RL_UPDATE_PHASE(rlIdx);

  // If FAST RECOVERY PHASE, then TIMER THERSHOLD is 10ms,
  //   if ACTIVE or HYPER-ACTIVE INCREASE then T/2 = 5ms
  if (RL[rlIdx].RLTimerCycle < FR_CYCLE_TH)
    expiredPeriod = TIMER_TH;
  else
    expiredPeriod = TIMER_TH/2;

  // Set TIMER 
  ScheduleTimer(rlIdx, expiredPeriod, false);
}

void
QCNRPQueue::ScheduleTimer(int rlIdx, double expiredPeriod, bool resetEvent)
{
  NS_LOG_FUNCTION (this);

  uint64_t expiredPeriodMS = (uint64_t)(expiredPeriod);

  // Received Fb, so reset scheduled events
  if (resetEvent)
    RL[rlIdx].RLTimer.Remove();

  // Do not resechedule while running
  if (RL[rlIdx].RLTimer.IsRunning())
    return;

  // Schedule RL:TIMER
  RL[rlIdx].RLTimer.SetFunction (&QCNRPQueue::RL_TIMER, this);
  RL[rlIdx].RLTimer.SetArguments (rlIdx);
  RL[rlIdx].RLTimer.Schedule (MilliSeconds(expiredPeriodMS));
}

// QCN-TRAFFIC-SHAPER----------------------------------------------------------
bool
QCNRPQueue::tsBacklogEnqueue (Ptr<Packet> p)
{
  // drop packet if the backlog is full
  if ((tsBacklogQsize + p->GetSize ()) > tsBacklogMaxQsize)
    return false;

  // update backlog Q
  tsBacklogQsize += p->GetSize ();
  tsBacklogQ.push (p);

  // start traffic shapper is necessary
  if (!shapperRunning)
    TrafficShaper();

  return true;
}

void
QCNRPQueue::TrafficShaper(void)
{
  Ptr<Packet> p;
  double tshTime = 1/100000000;

  if (!tsBacklogQ.empty ()) {
    bool dropPkt   = false;
    shapperRunning = true;

    p = tsBacklogQ.front ();
    tsBacklogQsize -= p->GetSize ();
    tsBacklogQ.pop ();

    // Check if the TX queue will be full
    if ((m_mode == QUEUE_MODE_PACKETS && (m_packets.size () >= m_maxPackets)) ||
        (m_mode == QUEUE_MODE_BYTES && (m_bytesInQueue + p->GetSize () >= m_maxBytes))) {
        std::cout << "RP: Queue full (at max packets:"
                  << m_maxPackets << ") -- droppping pkt" << std::endl;

        Drop (p);
        dropPkt = true;
    } else {
      m_bytesInQueue += p->GetSize ();
      m_packets.push (p);

      // this is part of the shit... inform upclass Q that it has frames to consume
      rpStatsUpdate(p);

      int flowID = GetFlowID(p);
      int rlIdx = GetRLIdx(flowID);
      RL[rlIdx].qlen = m_bytesInQueue;

      // LOG: log current rate
      vbrF = fopen(vbrFileString.c_str(), "a");
      fprintf(vbrF, "%ld  %lu\n", Simulator::Now().GetMicroSeconds(), (uint64_t)(RL[rlIdx].CR/1000000));
      fclose(vbrF);

      // RATE LIMITER BYTE COUTER update
      RL_BYTE_COUNTER(p);
    }

    tshTime = TrafficShaperSpeed(p, dropPkt);

    // Shaping traffic
    Time tEvent = Seconds (tshTime);
    Simulator::Schedule (tEvent, &QCNRPQueue::TrafficShaper, this);
  } else
    shapperRunning = false;
}

double
QCNRPQueue::TrafficShaperSpeed(Ptr<Packet> p, bool dropPkt)
{

  // Get Constant Bit Rate of the line (csma channel)
  Ptr<CsmaNetDevice> csmaNetDevice = m_qnetdev->GetObject<CsmaNetDevice> ();
  Ptr<Channel> channel = csmaNetDevice->GetChannel();
  Ptr<CsmaChannel> csmaChannel = channel->GetObject<CsmaChannel> ();
  DataRate c_cbr = csmaChannel->GetDataRate();

  int flowID = GetFlowID(p);
  int rlIdx = GetRLIdx(flowID);
  DataRate vbr(RL[rlIdx].CR);

  // Compute Variable Bit Rate according to RL:CR
  double tshTime = (vbr.CalculateTxTime (p->GetSize ()) - c_cbr.CalculateTxTime (p->GetSize ()));
  if (dropPkt || tshTime <= 0)
	tshTime = c_cbr.CalculateTxTime (p->GetSize ());

  return tshTime;
}

} // namespace ns3

