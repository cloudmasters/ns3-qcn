#ifndef QCN_FB_PROTO_H_H
#define QCN_FB_PROTO_H_H

#include "ns3/packet.h"
#include "ns3/queue.h"

#include "ns3/node.h"
#include "ns3/backoff.h"
#include "ns3/address.h"
#include "ns3/net-device.h"
#include "ns3/callback.h"
#include "ns3/packet.h"
#include "ns3/traced-callback.h"
#include "ns3/nstime.h"
#include "ns3/data-rate.h"
#include "ns3/ptr.h"
#include "ns3/mac48-address.h"

namespace ns3 {

#define QCN_PROTO_NO 0xFAC

class QCNFbHeader : public Header 
{
public:

  QCNFbHeader ();
  virtual ~QCNFbHeader ();

  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;
  virtual void Print (std::ostream &os) const;
  virtual void Serialize (Buffer::Iterator start) const;
  virtual uint32_t Deserialize (Buffer::Iterator start);
  virtual uint32_t GetSerializedSize (void) const;

  void SetFlowID(uint8_t flowID);
  void SetQnzFb(uint8_t QnzFb);
  uint8_t GetFlowID(void);
  uint8_t GetQnzFb();
  uint16_t GetQCNTag (void) const;
private:
  uint16_t m_data;
};

} // namespace ns3

#endif /* QCN_FB_PROTO_H_H */
