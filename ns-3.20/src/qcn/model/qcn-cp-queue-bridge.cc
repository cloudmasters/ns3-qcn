#include <math.h>

#include "ns3/log.h"
#include "ns3/enum.h"
#include "ns3/uinteger.h"

#include "ns3/qcn-fb-proto.h"
#include "ns3/ethernet-header.h"
#include "ns3/net-device.h"
#include "ns3/csma-net-device.h"
#include "ns3/channel.h"
#include "ns3/qcn-cp-queue.h"

#include <stdio.h>
#include <stdlib.h>

#define CP_LOG
#define CP_ON_INPUT

NS_LOG_COMPONENT_DEFINE ("QCNCPQueue");

namespace ns3 {

NS_OBJECT_ENSURE_REGISTERED (QCNCPQueue);

TypeId QCNCPQueue::GetTypeId (void) 
{
  static TypeId tid = TypeId ("ns3::QCNCPQueue")
    .SetParent<Queue> ()
    .AddConstructor<QCNCPQueue> ()
    .AddAttribute ("MaxBytes", 
                   "The maximum number of bytes accepted by this QCNCPQueue.",
                   UintegerValue (100 * 65535),
                   MakeUintegerAccessor (&QCNCPQueue::m_maxBytes),
                   MakeUintegerChecker<uint32_t> ())
  ;
  return tid;
}

QCNCPQueue::QCNCPQueue () :
  Queue (),
  m_packets (),
  m_bytesInQueue (0)
{
  NS_LOG_FUNCTION (this);
  m_mode = QUEUE_MODE_BYTES;

  // QCN parameters initialization
  isCPActive = false;

  m_Qeq = 33000; // Asuming: Q-MAX-LEN = 1500*100bytes
  m_W = 2;
  m_Q = 0;
  m_Qold = 0;
#define SAMPLING_RATE 1500 /* MTU */
  timeToMark = SAMPLING_RATE;
//  sampling = 1000000;/* 1s    = 1000000us */
//  sampling = 500000; /* 0.5s  = 500000us */
//  sampling = 250000; /* 0.25s = 250000us */
//  sampling = 100000; /* 0.10s = 100000us */
  sampling = 10000; /* 10ms = 10000us */
}

QCNCPQueue::~QCNCPQueue ()
{
  NS_LOG_FUNCTION (this);
}

void
QCNCPQueue::SetNetDev(Ptr<NetDevice> QnetDev)
{
  NS_LOG_FUNCTION (this);
  m_qnetdev = QnetDev;

  // Logging
  // ---------------------------------------------------------------------
  // write throughput data to files - each file name is the MAC address:
  //   e.g. 00000e or 000AABBFF... etc
  Ptr<CsmaNetDevice> csmaNetDevice = m_qnetdev->GetObject<CsmaNetDevice> ();
  Address csmaNetDevAddr = csmaNetDevice->GetAddress();
  uint8_t csmaNetDevAddrBuf[64];
  uint32_t addrLen = csmaNetDevAddr.CopyTo(csmaNetDevAddrBuf);
  
  std::stringstream convertToString;
  for (uint32_t y=0; y<addrLen; y++)
   convertToString<<((uint32_t)csmaNetDevAddrBuf[y]);

  queueFileString = "./qcn/cp_";
  queueFileString.append(convertToString.str());
  queueFileString.append("_queue");

  logFileString = "./qcn/cp_";
  logFileString.append(convertToString.str());
  logFileString.append("_log");
}

void
QCNCPQueue::SetQeq (int Qeq_set)
{
  NS_LOG_FUNCTION (this);
  m_Qeq = Qeq_set;
}

int
QCNCPQueue::GetQeq (void)
{
  NS_LOG_FUNCTION (this);
  return m_Qeq;
}

void
QCNCPQueue::SetW (int W)
{
  NS_LOG_FUNCTION (this);
  m_W = W;
}

int
QCNCPQueue::GetW (void)
{
  NS_LOG_FUNCTION (this);
  return m_W;
}

bool 
QCNCPQueue::DoEnqueue (Ptr<Packet> p)
{
  NS_LOG_FUNCTION (this << p);

  // QCN sampling
#if defined (CP_ON_INPUT)
  if (isCPActive && QCN_ComputeFeedback(p))
    QCN_SendFeedback(p);

  rpLogQSize();
#endif

  if (m_mode == QUEUE_MODE_PACKETS && (m_packets.size () >= m_maxPackets))
    {
      std::cout << "QUEUE" << logFileString.c_str()
                << ":CP: Queue full (at max packets:"
                << m_maxPackets << ") -- droppping pkt" << std::endl;
      NS_LOG_LOGIC ("Queue full (at max packets) -- droppping pkt");
      Drop (p);
      return false;
    }

  if (m_mode == QUEUE_MODE_BYTES && (m_bytesInQueue + p->GetSize () >= m_maxBytes))
    {
      std::cout << "QUEUE" << logFileString.c_str();
      std::cout << ":CP: Queue full (packet would exceed max bytes"
                << m_maxBytes << ") -- droppping pkt" << std::endl;
      NS_LOG_LOGIC ("Queue full (packet would exceed max bytes) -- droppping pkt");
      Drop (p);
      return false;
    }

  // Add the packet's flow if not found - RANKing
  QCN_UpdateFlowTable(p);

  m_bytesInQueue += p->GetSize ();
  m_packets.push (p);

  NS_LOG_LOGIC ("Number packets " << m_packets.size ());
  NS_LOG_LOGIC ("Number bytes " << m_bytesInQueue);

  return true;
}

Ptr<Packet>
QCNCPQueue::DoDequeue (void)
{
  NS_LOG_FUNCTION (this);

  if (m_packets.empty ())
    {
      NS_LOG_LOGIC ("Queue empty");
      return 0;
    }

  Ptr<Packet> p = m_packets.front ();
  m_packets.pop ();
  m_bytesInQueue -= p->GetSize ();

#if !defined(CP_ON_INPUT)
  // QCN sampling
  if (isCPActive && QCN_ComputeFeedback(p))
    QCN_SendFeedback(p);
  rpLogQSize();
#endif

  NS_LOG_LOGIC ("Popped " << p);

  NS_LOG_LOGIC ("Number packets " << m_packets.size ());
  NS_LOG_LOGIC ("Number bytes " << m_bytesInQueue);

  return p;
}

Ptr<const Packet>
QCNCPQueue::DoPeek (void) const
{
  NS_LOG_FUNCTION (this);

  if (m_packets.empty ())
    {
      NS_LOG_LOGIC ("Queue empty");
      return 0;
    }

  Ptr<Packet> p = m_packets.front ();

  NS_LOG_LOGIC ("Number packets " << m_packets.size ());
  NS_LOG_LOGIC ("Number bytes " << m_bytesInQueue);

  return p;
}


// QCN - CP -------------------------------------------------------------------
//-----------------------------------------------------------------------------
void 
QCNCPQueue::ActiveCP(bool isCP)
{
  isCPActive = isCP;
  return;
}
#err
bool
QCNCPQueue::QCN_ComputeFeedback(Ptr<Packet> p)
{
  NS_LOG_FUNCTION (this);

  bool sendFb = false;
  int Fb_max = 0;

  // Compute Fb: !Mind that Fb si computed as positive, rather than -Fb
  m_Q      = m_bytesInQueue + p->GetSize ();
  m_Qdelta = m_Q - m_Qold;
  m_Qoff   = m_Q - m_Qeq;
  Fb       = m_Qoff + m_W*m_Qdelta;
  Fb_max   = m_Qeq*(2*m_W + 1);

  // The maximum value of Fb determines the number of bits that Fb uses.
  // Uniform quantization of Fb, qntz_Fb, uses most significant bits of Fb.
  qntz_Fb = 0;
  if (Fb > Fb_max)
    Fb = Fb_max;
  else if (Fb > 0)
    qntz_Fb = 63*Fb/Fb_max;

#if defined(CP_LOG)
	logFile = fopen(logFileString.c_str(), "a");
		fprintf(logFile, "--> CP Simu at:%ldus\n", Simulator::Now().GetMicroSeconds());
		fprintf(logFile,"  > Q:%d\n", m_Q);
		fprintf(logFile,"  > Qold:%d\n", m_Qold);
		fprintf(logFile,"  > Fb:%d\n", Fb);
		fprintf(logFile,"  > qntzFb:%d\n", qntz_Fb);
		fprintf(logFile, " >\n");
	fclose(logFile);
#endif

  // Sampling rate: 1.5KB
  timeToMark -= p->GetSize ();
  if (timeToMark <= 0 && qntz_Fb > 0) {
    timeToMark = SAMPLING_RATE;
    m_Qold = m_Q;

    if (qntz_Fb > 0)
      sendFb = true;
  }

  // QCN stats
  QCN_UpdateStats(p); 

  if (sendFb) {
#if defined(CP_LOG)
	EthernetHeader pEthH;
	p->PeekHeader(pEthH);
	Mac48Address pMacSRC = pEthH.GetSource();
	uint8_t srcMacBuf[6];
	pMacSRC.CopyTo(srcMacBuf);

	logFile = fopen(logFileString.c_str(), "a");
		fprintf(logFile, "--> Feedback to:");
		for ( int t=0; t<6; t++)
			fprintf(logFile, "%u", srcMacBuf[t]);
		fprintf(logFile, " at:%ldus\n", Simulator::Now().GetMicroSeconds());
		fprintf(logFile,"  > Q         :%d\n", m_Q);
		fprintf(logFile,"  > Fb        :%d\n", Fb);
		fprintf(logFile,"  > qntz_Fb   :%d\n", qntz_Fb);
		fprintf(logFile,"--> Feedback-SENT\n");
	fclose(logFile);
#endif
    return true;
  }

  return false;
}

void
QCNCPQueue::QCN_SendFeedback(Ptr<Packet> p)
{
  EthernetHeader pEthH;
  p->PeekHeader(pEthH);
  Mac48Address pMacSRC = pEthH.GetSource();

  // prepare QCN Fb packet
  Ptr<Packet> Fbp = Create<Packet> ();
  QCNFbHeader FbQCNH;
  uint8_t srcMacBuf[6];
  pMacSRC.CopyTo(srcMacBuf);
  FbQCNH.SetFlowID(0);
  FbQCNH.SetQnzFb(qntz_Fb);
  Fbp->AddHeader(FbQCNH);

  //Get the BridgeNetDevice to which this queue belongs to - do it only once
  //(even though in real life bridges can be dynamic in our case they are only initilised once)
  if(!m_brnetdev) {
    Ptr<Node> swNode = m_qnetdev->GetNode();//the switch itself
    Ptr<BridgeNetDevice> swBridge;
    //Iterate through the NetDevicies of the node, but only some are bridges
    //and only one bridge has this CP port associated
    for(uint32_t i=0; i<swNode->GetNDevices(); i++) {
      Ptr<NetDevice> swNetDevice = swNode->GetDevice(i);
      if(swNetDevice->IsBridge()){ //our candidate is a bridge!
        swBridge = swNetDevice->GetObject<BridgeNetDevice>(); //N:not allways safe, but ok for now
        for(uint32_t j=0; j<swBridge->GetNBridgePorts(); j++) {
          if(swBridge->GetBridgePort(j) == m_qnetdev) { //found our bridge!
            m_brnetdev = swBridge;
            break;
          }
        }
      }
      if(m_brnetdev) break;
    }

    if(!m_brnetdev) {
      std::cout << "BridgeNetDevice not found... "
                << "this device is not part of bridge..."
                << "where should I send the feedback? Add the dev to a BridgeNetDevice" << std::endl;
      exit(1);
    }
  }

  //Send the packet to the BridgeNetDevice for processing and forwarding on the correct port
  //Note: this simulates sending a message from the CPU to the FDB
  m_brnetdev->Send (Fbp,      //the packet with Fb
                    pMacSRC,  //the initial packet MACSRC is the Fb MACDST
                    0xFAC);   //invented by me for simulation
  return;
}

void
QCNCPQueue::QCN_UpdateFlowTable(Ptr<Packet> p)
{
  if (false == QCN_FindFlow(p))
    QCN_AddFlow(p);

  return;
}

bool
QCNCPQueue::QCN_FindFlow(Ptr<Packet> p)
{
  if (FlowFbStats.empty())
    return false;

  bool foundMac = true;

  // Flow = MAC SRC
  EthernetHeader pEthH;
  p->PeekHeader(pEthH);
  Mac48Address pMacSRC = pEthH.GetSource();
  uint8_t srcMacBuf[6];
  pMacSRC.CopyTo(srcMacBuf);

  // seek flow id
  for (FlowFbStats_it_t it = FlowFbStats.begin(); it != FlowFbStats.end(); ++it){
    uint8_t itBuf[6];
    it->first.CopyTo(itBuf);

    foundMac = true;
    for ( int t=0; t<6; t++) {
      if(itBuf[t] != srcMacBuf[t]) {
        foundMac = false;
        continue;
      }
    }

    if (foundMac == true)
      break;
  }

  return foundMac;
}

void
QCNCPQueue::QCN_AddFlow(Ptr<Packet> p)
{
  EthernetHeader pEthH;
  p->PeekHeader(pEthH);
  Mac48Address pMacSRC = pEthH.GetSource();

  // Create new hash table for statistics and add it to FlowID has table
  FlowFbStats.insert(std::pair<Mac48Address, FbHashTable_t *>(pMacSRC, new FbHashTable_t));

#if 0
	uint8_t srcMacBuf[6];
	pMacSRC.CopyTo(srcMacBuf);
	std::cout << "QUEUE:" << logFileString.c_str()
		  << ":add FLow:";
		for ( int t=0; t<6; t++)
			std::cout << (uint32_t)srcMacBuf[t];
	std::cout << ":size(RANK):" << FlowFbStats.size() << std::endl;
#endif

  // Share is 0 for the newly added flow
  FlowShare.insert(std::pair<Mac48Address,int64_t> (pMacSRC, 0));
  q_Rank = FlowFbStats.size();

  return;
}

void
QCNCPQueue::QCN_AgingStats(Mac48Address pMacSRC, int64_t c_time)
{
  
  int64_t interval = c_time-sampling;

  if (interval < 0)
    return;

  FbHashTable_t *FbHashTable = FlowFbStats[pMacSRC];
  for (FbHashTable_it_t it = FbHashTable->begin(); it != FbHashTable->end(); ++it){
    if (it->first < interval)
      FbHashTable->erase(it);
  }
  return;
}

void
QCNCPQueue::QCN_UpdateStats(Ptr<Packet> p)
{
  int64_t c_time= Simulator::Now().GetMicroSeconds();

  // Flow = MAC SRC
  EthernetHeader pEthH;
  p->PeekHeader(pEthH);
  Mac48Address pMacSRC = pEthH.GetSource();

  // add new statistic for current flowID
  FbHashTable_t *FbHashTable = FlowFbStats[pMacSRC];
  FbHashTable->insert(std::pair<int64_t, int> (c_time, Fb));

  // Aging (remove statistics less than c_time-sampling)
  QCN_AgingStats(pMacSRC, c_time);

  // compute CP-Q SHARE
  int64_t SharingFlow = 0;
  int64_t Times = 0;
  FbHashTable_it_t first_it = FbHashTable->begin();
  for (FbHashTable_it_t it = FbHashTable->begin(); it != FbHashTable->end(); ++it){
    // normalize time
    int64_t nTime = it->first - first_it->first;

    SharingFlow += ((int64_t)(it->second)*nTime);
    Times += nTime;
  }

  // compute SHARE
  if (Times)
    FlowShare[pMacSRC] = (int64_t)((SharingFlow*q_Rank)/Times);
  else
    FlowShare[pMacSRC] = 0;

#if 1
	std::cout << "QUEUE:"  << logFileString.c_str()
		  << ":FlowID:";
		uint8_t srcMacBuf[6];
		pMacSRC.CopyTo(srcMacBuf);
		for ( int t=0; t<6; t++)
			std::cout << (uint32_t)srcMacBuf[t];
	std::cout << ":Flow Share:" << FlowShare[pMacSRC] 
		  << ":RANK:" << q_Rank << std::endl;
#endif

  return;
}

void 
QCNCPQueue::rpLogQSize(void)
{
  queueFile = fopen(queueFileString.c_str(), "a");
  fprintf(queueFile, "%ld %d\n", Simulator::Now().GetMicroSeconds() ,m_bytesInQueue);
  fclose(queueFile);
}

} // namespace ns3

