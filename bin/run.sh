#/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Usage is: ./run.sh <program-name>"
    exit 1
fi

#get directory I'm in
CWD=`pwd`
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#build
cd $DIR/../ns-3.20/
echo "Sending output also to: `pwd`/../log/output.out"
export 'NS_LOG=*=level_info|prefix_func|prefix_time'
./waf --run $1 2>&1 | tee ../log/output.out

#get back to current dir
cd $CWD
