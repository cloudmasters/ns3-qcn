#/bin/bash

#get directory I'm in
CWD=`pwd`
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#build
cd $DIR/../ns-3.20/
./waf build

#get back to current dir
cd $CWD
