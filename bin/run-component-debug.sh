#/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Usage is: ./run.sh <program-name> <component-name>"
    exit 1
fi

#get directory I'm in
CWD=`pwd`
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#build
cd $DIR/../ns-3.20/
echo "Sending output also to: `pwd`/../log/output.out"
LOG='level_all|prefix_func|prefix_time'
export NS_LOG="$2=$LOG"
echo $NS_LOG
./waf --run $1 2>&1 | tee ../log/output.out

#get back to current dir
cd $CWD
