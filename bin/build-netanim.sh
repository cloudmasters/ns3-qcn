#/bin/bash

#get directory I'm in
CWD=`pwd`
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#build
cd $DIR/../netanim-3.105/
#sudo apt-get install qt-sdk
sudo apt-get install qt4-qmake libqt4-dev
make clean
qmake-qt4 NetAnim.pro
make

#get back to current dir
cd $CWD
