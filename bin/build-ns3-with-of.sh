#/bin/bash

#get directory I'm in
CWD=`pwd`
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#build
cd $DIR/../ns-3.20/
./waf configure --enable-examples --enable-tests --with-openflow=../openflow/

#get back to working dir
cd $CWD
