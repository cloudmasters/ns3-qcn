#/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Usage is: ./run-full-debug.sh <program-name>"
    exit 1
fi

#get directory I'm in
CWD=`pwd`
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#build
cd $DIR/../ns-3.20/
echo "Outputing everything to: `pwd`/../log/full-output.out"
export 'NS_LOG=*=level_all|prefix_func|prefix_time'
./waf --run $1 > ../log/full-output.out 2>&1

#get back to current dir
cd $CWD
